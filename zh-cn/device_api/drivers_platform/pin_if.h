/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */

/**
 * @file pin_if.h
 *
 * @brief 声明标准pin接口函数。
 *
 * @since 3.1
 */

#ifndef PIN_IF_H
#define PIN_IF_H

#include "platform_if.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 枚举管脚推拉方式。
 *
 * @since 3.1
 */
enum PinPullType {
    /** 设置管脚为悬空。 */
    PIN_PULL_NONE = 0,
    /** 设置管脚为上拉。 */
    PIN_PULL_UP =  1,
    /** 设置管脚为下拉。 */
    PIN_PULL_DOWN = 2,
};

/**
 * @brief 获取一个管脚描述句柄。
 * 在设置管脚属性之前，必须调用此函数。
 *
 * @param pinName 表示要设置管脚属性的管脚名。
 *
 * @return 管脚描述句柄 表示操作成功。
 * @return NULL 表示操作失败。
 *
 * @since 3.1
 */
DevHandle PinGet(const char *pinName);

/**
 * @brief 释放管脚描述句柄。
 * 如果不再需要设置管脚，则应调用此函数释放该管脚描述句柄，释放未使用的内存资源。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 *
 * @since 3.1
 */
void PinPut(DevHandle handle);

/**
 * @brief 设置管脚推拉方式。
 * 当需要设置管脚推拉方式时，可以调用该函数。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 * @param pullType 表示管脚推拉方式。
 *
 * @return 0 表示操作成功
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t PinSetPull(DevHandle handle, enum PinPullType pullType);

/**
 * @brief 获取管脚推拉方式。
 * 当需要获取管脚推拉方式时，可以调用该函数。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 * @param pullType 表示管脚推拉方式的指针。
 *
 * @return 0 表示操作成功
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t PinGetPull(DevHandle handle, enum PinPullType *pullType);

/**
 * @brief 设置管脚推拉强度。
 * 当需要设置管脚推拉强度时，可以调用该函数。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 * @param strength 表示管脚推拉强度的值。
 *
 * @return 0 表示操作成功
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t PinSetStrength(DevHandle handle, uint32_t strength);

/**
 * @brief 获取管脚推拉强度。
 * 当需要获取管脚推拉强度时，可以调用该函数。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 * @param strength 表示管脚推拉强度值的指针。
 *
 * @return 0 表示操作成功
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t PinGetStrength(DevHandle handle, uint32_t *strength);

/**
 * @brief 设置管脚功能。
 * 当需要设置管脚功能时，可以调用该函数。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 * @param funcName 表示管脚功能的指针。
 *
 * @return 0 表示操作成功
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t PinSetFunc(DevHandle handle, const char *funcName);

/**
 * @brief 获取管脚功能。
 * 当需要获取管脚功能时，可以调用该函数。
 *
 * @param handle 表示指向管脚描述句柄的指针。
 * @param funcName 表示管脚功能的二重指针。
 *
 * @return 0 表示操作成功
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t PinGetFunc(DevHandle handle, const char **funcName);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* PIN_IF_H */
