/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Codec模块接口定义。
 *
 * Codec模块涉及自定义类型、音视频编解码组件初始化、参数设置、数据的轮转和控制等。
 *
 * @since 4.1
 * @version 2.0
 */

/**
 * @file ICodecComponent.idl
 *
 * @brief 主要包括Codec组件接口定义。
 *
 * Codec模块提供了获取组件信息、给组件发送命令、组件参数设置、buffer轮转和控制等接口定义。创建组件后，可使用下列接口进行编解码处理。
 *
 * @since 4.1
 * @version 2.0
 */
 
/**
 * @brief Codec模块接口的包路径。
 *
 * @since 4.1
 * @version 2.0
 */
package ohos.hdi.codec.v2_0;

import ohos.hdi.codec.v2_0.CodecTypes;
import ohos.hdi.codec.v2_0.ICodecCallback;

/**
 * @brief Codec组件接口定义。
 *
 * 主要提供以下功能：
 * - 获取组件的版本
 * - 组件参数配置的获取和设置
 * - 发送命令至组件及获取组件状态
 * - 设置回调函数
 * - 设置/释放组件使用的buffer
 * - 编解码输入输出buffer处理
 * 具体方法使用详见函数说明。
 */

interface ICodecComponent {
    /**
     * @brief 获取Codec组件版本号。
     *
     * 通过查询组件，返回组件版本信息。
     *
     * @param verInfo 指向组件版本信息的对象，详见{@link CompVerInfo}。
     *
     * @return HDF_SUCCESS 表示获取版本号成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取版本号失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    GetComponentVersion([out] struct CompVerInfo verInfo);

    /**
     * @brief 发送命令给组件。
     *
     * 发送命令给组件，当命令为设置状态时，会有事件回调通知结果给上层，其他命令则没有事件上报。
     *
     * @param cmd 组件要执行的命令，详见{@link OMX_COMMANDTYPE}。
     * @param param 组件要执行的命令携带的参数。
     * - 当cmd为OMX_CommandStateSet时，param的值详见{@link OMX_STATETYPE}。
     * - 当cmd为OMX_CommandFlush、OMX_CommandPortDisable、OMX_CommandPortEnable、OMX_CommandMarkBuffer时，param为目标端口。
     * @param cmdData 当cmd为OMX_CommandMarkBuffer时，指向OMX_MARKTYPE结构体指针。
     *
     * @return HDF_SUCCESS 表示发送命令成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，发送命令失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    SendCommand([in] enum OMX_COMMANDTYPE cmd, [in] unsigned int param, [in] byte[] cmdData);

    /**
     * @brief 获取组件参数设置。
     *
     * 当组件处于除了OMX_StateInvalid（组件状态异常）之外的其他状态，用户可通过此接口获取组件参数，组件状态详见{@link OMX_STATETYPE}。
     *
     * @param index 待填充结构的索引，详见OMX IL定义的OMX_INDEXTYPE。
     * @param inParamStruct 指向由组件填充的应用程序分配的结构体指针。
     * @param outParamStruct 指向由组件填充的应用程序分配的结构体指针。
     *
     * @return HDF_SUCCESS 表示获取参数成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取参数失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    GetParameter([in] unsigned int index, [in] byte[] inParamStruct, [out] byte[] outParamStruct);

    /**
     * @brief 设置组件需要的参数。
     *
     * 当出现如下情况时，用户可以通过此接口设置组件参数。
     * - 当组件处于OMX_StateLoaded（表示组件已加载）。
     * - 当组件处于OMX_StateWaitForResources（表示组件等待所需要的资源）。
     * - 当状态或者端口是去使能状态，用户可通过此接口设置组件参数。
     * 更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param index 要设置的结构索引，详见OMX IL定义的OMX_INDEXTYPE。
     * @param paramStruct 指向组件用于初始化的应用程序分配结构的指针。
     *
     * @return HDF_SUCCESS 表示设置参数成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，设置参数失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    SetParameter([in] unsigned int index, [in] byte[] paramStruct);

    /**
     * @brief 获取组件的配置。
     *
     * 加载组件后可以随时调用此接口获取组件的配置。
     *
     * @param index 待填充结构的索引，详见{@link OMX_INDEXTYPE}。
     * @param inCfgStruct 指向由组件填充的应用程序分配的结构体指针。
     * @param outCfgStruct 指向由组件填充的应用程序分配的结构体指针。
     *
     * @return HDF_SUCCESS 表示获取配置成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取配置失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    GetConfig([in] unsigned int index, [in] byte[] inCfgStruct, [out] byte[] outCfgStruct);

    /**
     * @brief 设置组件的配置。
     *
     * 加载组件后可以随时调用此接口设置组件的配置。
     *
     * @param index 要设置的结构索引，详见{@link OMX_INDEXTYPE}。
     * @param cfgStruct 指向组件用于初始化的应用程序分配结构的指针。
     *
     * @return HDF_SUCCESS 表示设置配置成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，设置失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    SetConfig([in] unsigned int index, [in] byte[] cfgStruct);

    /**
     * @brief 根据字符串获取组件的扩展索引。
     *
     * 将扩展字符串转换为Openmax IL结构索引，此索引可用于获取（{@link GetParameter}）或者设置（{@link SetParameter}）组件参数。
     *
     * @param paramName 组件用来转换为配置索引的字符串。
     * @param indexType 由paramName转换的配置索引，详见{@link OMX_INDEXTYPE}。
     *
     * @return HDF_SUCCESS 表示获取扩展索引成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取扩展索引失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    GetExtensionIndex([in] String paramName, [out] unsigned int indexType);

    /**
     * @brief 获取组件的当前状态。
     *
     * 用户可调用此接口获取组件的当前状态。
     *
     * @param state 指向获取到的状态指针，组件状态详见{@link OMX_STATETYPE}。
     *
     * @return HDF_SUCCESS 表示获取状态成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取状态失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    GetState([out] enum OMX_STATETYPE state);

    /**
     * @brief 设置组件采用Tunnel方式通信。
     *
     * 当组件处于OMX_StateLoaded状态时（表示组件已加载），用户通过调用此接口确定组件是否可以进行Tunnel传输，如果可以则设置组件的Tunnel传输。
     * 更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param port 组件设置的端口。
     * @param tunneledComp 组件的tunnel组件句柄。
     * @param tunneledPort 组件用来Tunnel通信的端口。
     * @param inTunnelSetup 指向Tunnel设置的结构体{@link OMX_TUNNELSETUPTYPE}指针。
     * @param outTunnelSetup 指向Tunnel设置的结构体{@link OMX_TUNNELSETUPTYPE}指针。
     *
     * @return HDF_SUCCESS 表示设置成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，设置失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    ComponentTunnelRequest([in] unsigned int port, [in] int tunneledComp, [in] unsigned int tunneledPort, [in] struct OMX_TUNNELSETUPTYPE inTunnelSetup, [out] struct OMX_TUNNELSETUPTYPE outTunnelSetup);

    /**
     * @brief 指定组件端口的buffer。
     *
     * 此接口在以下情况下使用：
     * - 当组件处于OMX_StateLoaded状态（表示组件已加载），并且用户已经向组件发送OMX_StateIdle状态转换请求。
     * - 当组件处于OMX_StateWaitForResources状态，所需的资源可用，并且组件已准备好进入OMX_StateIdle状态。
     * - 在去使能端口上，组件处于OMX_StateExecuting、OMX_StatePause或OMX_StateIdle状态。
     * 更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param portIndex 指定的组件端口。
     * @param inBuffer 指向要使用的buffer结构体的指针，结构体介绍详见{@link OmxCodecBuffer}。
     * @param outBuffer 指向要使用的buffer结构体的指针，结构体介绍详见{@link OmxCodecBuffer}。
     *
     * @return HDF_SUCCESS 表示指定成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，指定失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    UseBuffer([in] unsigned int portIndex, [in] struct OmxCodecBuffer inBuffer, [out] struct OmxCodecBuffer outBuffer);

    /**
     * @brief 向组件申请端口buffer。
     *
     * 向组件申请分配新的buffer，此接口在以下情况下使用：
     * - 当组件处于OMX_StateLoaded状态，并且用户已经向组件发送OMX_StateIdle状态转换请求。
     * - 当组件处于OMX_StateWaitForResources状态，所需的资源可用，并且组件已准备好进入OMX_StateIdle状态。
     * - 在去使能端口上，组件处于OMX_StateExecuting、OMX_StatePause或OMX_StateIdle状态。
     * 更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param portIndex 指定的组件端口。
     * @param inBuffer 指向要申请的buffer结构的体指针，结构体介绍详见{@link OmxCodecBuffer}。
     * @param outBuffer 指向要申请的buffer结构的体指针，结构体介绍详见{@link OmxCodecBuffer}。
     *
     * @return HDF_SUCCESS 表示申请buffer成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，申请buffer失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    AllocateBuffer([in] unsigned int portIndex, [in] struct OmxCodecBuffer inBuffer, [out] struct OmxCodecBuffer outBuffer);

    /**
     * @brief 释放buffer。
     *
     * 此接口在以下情况下使用：
     * - 当组件处于OMX_StateIdle状态，并且已经向组件发送OMX_StateLoaded状态转换请求。
     * - 在去使能端口上，组件处于OMX_StateExecuting、OMX_StatePause或OMX_StateIdle时调用。
     * 更多组件状态的说明请详见{@link OMX_STATETYPE}。
     * - 此接口调用可随时进行，但是如果未在上述情况下执行，可能会导致组件上报OMX_ErrorPortUnpopulated事件。
     *
     * @param portIndex 指定的组件端口。
     * @param buffer 指向要释放的buffer结构体的结构体的指针，结构体介绍详见{@link OmxCodecBuffer}。
     *
     * @return HDF_SUCCESS 表示释放buffer成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，释放buffer失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    FreeBuffer([in] unsigned int portIndex, [in] struct OmxCodecBuffer buffer);

    /**
     * @brief 编解码输入待处理buffer。
     *
     * 此接口在组件处于OMX_StateExecuting或者OMX_StatePause状态时调用，更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param buffer 指向要输入的buffer结构体的指针，结构体介绍详见{@link OmxCodecBuffer}。
     *
     * @return HDF_SUCCESS 表示输入buffer成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，输入buffer失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    EmptyThisBuffer([in] struct OmxCodecBuffer buffer);

    /**
     * @brief 编解码输出填充buffer。
     *
     * 此接口在组件处于OMX_StateExecuting或者OMX_StatePause状态时调用，更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param buffer 指向要填充的buffer结构体的指针，结构体介绍详见{@link OmxCodecBuffer}。
     *
     * @return HDF_SUCCESS 表示填充buffer成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，填充buffer失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    FillThisBuffer([in] struct OmxCodecBuffer buffer);

    /**
     * @brief 设置Codec组件的回调函数。
     *
     * 当组件处于OMX_StateLoaded状态时，使用此回调函数向上通知事件以及上报可用的输入输出信息。更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param callbacks 指向回调函数{@link ICodecCallback}对象的指针。
     * @param appData 指向应用程序定义的值的指针，该值将在回调期间返回。
     *
     * @return HDF_SUCCESS 表示设置回调成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，设置回调失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    SetCallbacks([in] ICodecCallback callbacks, [in] long appData);

    /**
     * @brief 组件去初始化。
     *
     * 调用此接口使组件去初始化，当组件处于OMX_StateLoaded状态时，将直接关闭组件，更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @return HDF_SUCCESS 表示去初始化成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，去初始化失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    ComponentDeInit();

    /**
     * @brief 使用已在EGL中申请的空间。
     *
     * 此接口在以下情况下使用：
     * - 当组件处于OMX_StateLoaded状态，并且已经向组件发送OMX_StateIdle状态转换请求。
     * - 当组件处于OMX_StateWaitForResources状态，所需的资源可用，并且组件已准备好进入OMX_StateIdle状态。
     * - 在去使能端口上，组件处于OMX_StateExecuting、OMX_StatePause或OMX_StateIdle状态。
     * 更多组件状态的说明请详见{@link OMX_STATETYPE}。
     *
     * @param portIndex 指定的组件端口。
     * @param inBuffer 指向{@link OmxCodecBuffer}结构体的指针。
     * @param outBuffer 指向{@link OmxCodecBuffer}结构体的指针。
     * @param eglImage  EGL申请的图像指针。
     *
     * @return HDF_SUCCESS 表示使用成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，使用失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    UseEglImage([in] unsigned int portIndex, [in] struct OmxCodecBuffer inBuffer, [out] struct OmxCodecBuffer outBuffer, [in] byte[] eglImage);

    /**
     * @brief 获取组件角色。
     *
     * 根据组件角色索引获取对应组件角色。
     *
     * @param role 角色名称。
     * @param index 角色的索引，一个组件可能支持多种角色。
     *
     * @return HDF_SUCCESS 表示获取角色成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取角色失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    ComponentRoleEnum([out] unsigned char[] role, [in] unsigned int index);
}
/** @} */