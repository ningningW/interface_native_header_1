/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Input
 * @{
 *
 * @brief Input模块驱动接口声明。
 *
 * 本模块为Input服务提供相关驱动接口，包括Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * @since 1.0
 */

/**
 * @file input_controller.h
 *
 * @brief 描述Input设备业务控制相关的接口声明。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef INPUT_CONTROLLER_H
#define INPUT_CONTROLLER_H
 
#include "input_type.h"
 
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供Input设备业务控制相关的接口。
 *
 * 此类接口包含电源状态的设置、特性的使能、器件信息的获取，以及产线相关的测试功能接口。
 */
typedef struct {
    /**
     * @brief 设置电源状态。
     *
     * 在系统休眠或者唤醒时，Input服务或电源管理模块设置电源状态，以使驱动IC能正常进入对应的休眠模式。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param status 输入参数，设置的电源状态，Input服务控制Input设备进入resume或者suspend等状态{@link PowerStatus}。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetPowerStatus)(uint32_t devIndex, uint32_t status);

    /**
     * @brief 获取电源状态。
     *
     * 在系统休眠或者唤醒时，Input服务或电源管理模块获取电源状态，以便驱动IC能正常进入对应的休眠模式。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param status 输出参数，获取的对应设备索引的电源状态，具体参考{@link PowerStatus}。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetPowerStatus)(uint32_t devIndex, uint32_t *status);

    /**
     * @brief 获取devIndex对应的Input设备的类型。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param deviceType 输出参数，获取的对应设备索引的设备类型，具体参考{@link InputDevType}。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetDeviceType)(uint32_t devIndex, uint32_t *deviceType);

    /**
     * @brief 获取器件对应的编码信息。
     *
     * 一款产品通常会有多家模组和Driver IC，上层应用如果关注具体器件型号，则通过此接口来获取。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param chipInfo 输出参数，获取的对应设备索引的器件编码信息。
     * @param length 输入参数，保存器件芯片信息的内存长度。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0 * @version 1.0
     */
    int32_t (*GetChipInfo)(uint32_t devIndex, char *chipInfo, uint32_t length);

    /**
     * @brief 获取devIndex对应的模组厂商名。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param vendorName 输出参数，获取的对应设备索引的模组厂商名。
     * @param length 输入参数，保存模组厂商名的内存长度。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetVendorName)(uint32_t devIndex, char *vendorName, uint32_t length);

    /**
     * @brief 获取devIndex对应的驱动芯片名。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param chipName 输出参数，获取的对应设备索引的驱动芯片名。
     * @param length 输入参数，保存驱动芯片名的内存长度。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetChipName)(uint32_t devIndex, char *chipName, uint32_t length);

    /**
     * @brief 设置手势模式。
     *
     * 上层应用开关手势模式，即设置手势模式的对应使能bit。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param gestureMode 输入参数，手势模式的开关状态。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*SetGestureMode)(uint32_t devIndex, uint32_t gestureMode);

    /**
     * @brief 执行容值自检测试。
     *
     * 启动不同检测场景下的容值自检测试，并获取测试结果，容值自检项由器件厂商自定义，
     * 一般包括RawData测试、 * 短路检测、开路检测、干扰检测、行列差检测等测试项。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param testType 输入参数，容值测试的测试类型，具体参考{@link CapacitanceTest}。
     * @param result 输出参数，容值测试的结果，成功则输出“SUCC”，失败则返回对应的错误提示。
     * @param length 输入参数，保存容值测试结果的内存长度。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RunCapacitanceTest)(uint32_t devIndex, uint32_t testType, char *result, uint32_t length);

    /**
     * @brief 执行拓展指令。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param cmd 输入参数，拓展指令数据包，包括指令编码及参数，具体参考{@link InputExtraCmd}。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RunExtraCommand)(uint32_t devIndex, InputExtraCmd *cmd);
} InputController;

#ifdef __cplusplus
}
#endif
#endif
/** @} */