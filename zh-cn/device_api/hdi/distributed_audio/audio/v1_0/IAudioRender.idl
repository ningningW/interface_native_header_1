/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义。
 *
 * 音频接口涉及数据类型、驱动加载接口、驱动适配器接口、音频播放接口、音频录音接口等。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IAudioRender.idl
 *
 * @brief Audio播放的接口定义文件。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @brief 音频接口的包路径。
 *
 * @since 4.1
 * @version 1.0
 */
package ohos.hdi.distributed_audio.audio.v1_0;

import ohos.hdi.distributed_audio.audio.v1_0.AudioTypes;
import ohos.hdi.distributed_audio.audio.v1_0.IAudioCallback;

/**
 * 提供音频播放支持的驱动能力，包括音频控制、音频属性、音频场景、音频音量、获取硬件延迟时间、播放音频帧数据等。
 *
 *
 * @since 4.1
 * @version 1.0
 */
interface IAudioRender {
    /**
     * @brief 获取音频硬件驱动的延迟时间。
     *
     * @param ms 获取的延迟时间（单位：毫秒）保存到ms中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetLatency([out] unsigned int ms);

    /**
     * @brief 向音频驱动中播放一帧输出数据（放音，音频下行数据）。
     *
     * @param frame 待写入的输出数据的音频frame。
     * @param replyBytes 实际写入的音频数据长度（字节数），获取后保存到replyBytes中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    RenderFrame([in] byte[] frame, [out] unsigned long replyBytes);

    /**
     * @brief 获取音频已输出的帧数。
     *
     * @param frames 获取的音频帧数保存到frames中，详请参考{@link AudioTimeStamp}。
     * @param time 获取的关联时间戳保存到time中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see RenderFrame
     *
     * @since 4.1
     * @version 1.0
     */
    GetRenderPosition([out] unsigned long frames, [out] struct AudioTimeStamp time);

    /**
     * @brief 设置一个音频的播放速度。
     *
     * @param speed 待设置的播放速度（倍速），例0.5、0.75、1.0、1.25、1.5、2.0。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetRenderSpeed
     *
     * @since 4.1
     * @version 1.0
     */
    SetRenderSpeed([in] float speed);

    /**
     * @brief 获取一个音频当前的播放速度。
     *
     * @param speed 获取的播放速度保存到speed中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SetRenderSpeed
     *
     * @since 4.1
     * @version 1.0
     */
    GetRenderSpeed([out] float speed);

    /**
     * @brief 设置音频播放的通道模式。
     *
     * @param mode 待设置的通道模式，详请参考{@link AudioChannelMode}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetChannelMode
     *
     * @since 4.1
     * @version 1.0
     */
    SetChannelMode([in] enum AudioChannelMode mode);

    /**
     * @brief 获取音频播放当前的通道模式。
     *
     * @param mode 获取的通道模式保存到mode中，详请参考{@link AudioChannelMode}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SetChannelMode
     *
     * @since 4.1
     * @version 1.0
     */
    GetChannelMode([out] enum AudioChannelMode mode);

    /**
     * @brief 注册音频回调函数，用于放音过程中缓冲区数据写、DrainBuffer完成通知。
     *
     * @param audioCallback 注册的回调函数，详请参考{@link IAudioCallback}。
     * @param cookie 回调函数的入参。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see RegCallback
     *
     * @since 4.1
     * @version 1.0
     */
    RegCallback([in] IAudioCallback audioCallback, [in] byte cookie);

    /**
     * @brief 排空缓冲区中的数据。
     *
     * @param type 播放结束的类型，详请参考{@link AudioDrainNotifyType}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see RegCallback
     *
     * @since 4.1
     * @version 1.0
     */
    DrainBuffer([out] enum AudioDrainNotifyType type);

    /**
     * @brief 判断是否支持清空缓冲区数据的功能。
     *
     * @param support 是否支持的状态保存到support中，true表示支持，false表示不支持。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    IsSupportsDrain([out] boolean support);
    /**
     * @brief 是否支持某个音频场景的配置。
     *
     * @param scene 待判断的音频场景描述符，详请参考{@link AudioSceneDescriptor}。
     * @param supported 是否支持的状态保存到supported中，true表示支持，false表示不支持。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SelectScene
     *
     * @since 4.1
     * @version 1.0
     */
    CheckSceneCapability([in] struct AudioSceneDescriptor scene, [out] boolean supported);

    /**
     * @brief 选择音频场景。
     *
     * <ul>
     *   <li>1. 选择一个非常具体的音频场景（应用场景和输出设备的组合），例如同样是使用手机中的喇叭作为输出设备。
     *     <ul>
     *       <li>在媒体播放场景scene为media_speaker。</li>
     *       <li>在语音通话免提场景scene为voice_speaker。</li>
     *     </ul>
     *   <li>2. 只是选择一个音频场景，例如使用场景为媒体播放（media）、电影播放（movie）、游戏播放（game）。</li>
     *   <li>3. 只是选择一个音频输出设备，例如输出设备为听筒（receiver）、喇叭（speaker）、有线耳机（headset）。</li>
     * </ul>
     *
     * @param scene 待设置的音频场景描述符，详请参考{@link AudioSceneDescriptor}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see CheckSceneCapability
     *
     * @since 4.1
     * @version 1.0
     */
    SelectScene([in] struct AudioSceneDescriptor scene);

    /**
     * @brief 设置音频的静音状态。
     *
     * @param mute 待设置的静音状态，true表示静音操作、false表示取消静音操作。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetMute
     *
     * @since 4.1
     * @version 1.0
     */
    SetMute([in] boolean mute);

    /**
     * @brief 获取音频的静音状态。
     *
     * @param mute 获取的静音状态保存到mute中，true表示静音操作、false表示取消静音操作。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SetMute
     *
     * @since 4.1
     * @version 1.0
     */
    GetMute([out] boolean mute);

    /**
     * @brief 设置一个音频流的音量。
     *
     * 音量的取值范围是0.0~1.0，如果音频服务中的音量等级为15级（0 ~ 15），
     * 则音量的映射关系为0.0表示静音，1.0表示最大音量等级（15）。
     *
     * @param volume 待设置的音量，范围0.0~1.0。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetVolume([in] float volume);

    /**
     * @brief 获取一个音频流的音量。
     *
     * @param volume 获取的音量保存到volume中，范围0.0~1.0。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SetVolume
     *
     * @since 4.1
     * @version 1.0
     */
    GetVolume([out] float volume);

    /**
     * @brief 获取音频流增益的阈值。
     *
     * 在具体的功能实现中，可以根据芯片平台的实际情况来进行处理：
     *
     * <ul>
     *   <li>1. 可以使用实际的增益值，例如增益的范围为-50db ~ 6db。</li>
     *   <li>2. 也可以将增益范围设定为0.0~1.0，如果增益的范围为-50db ~ 6db，
     *          则增益的映射关系为0.0表示静音，1.0表示最大增益（6db）。</li>
     * </ul>
     *
     * @param min 获取的音频增益的阈值下限保存到min中。
     * @param max 获取的音频增益的阈值上限保存到max中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetGain
     * @see SetGain
     *
     * @since 4.1
     * @version 1.0
     */
    GetGainThreshold([out] float min, [out] float max);

    /**
     * @brief 获取音频流的增益。
     *
     * @param gain 保存当前获取到的增益到gain中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetGainThreshold
     * @see SetGain
     *
     * @since 4.1
     * @version 1.0
     */
    GetGain([out] float gain);

    /**
     * @brief 设置音频流的增益。
     *
     * @param gain 待设置的增益，最小为0.0，最大为1.0。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetGainThreshold
     * @see GetGain
     *
     * @since 4.1
     * @version 1.0
     */
    SetGain([in] float gain);

    /**
     * @brief 获取音频帧的大小。
     *
     * 获取一帧音频数据的长度（字节数）。
     *
     * @param size 获取的音频帧大小（字节数）保存到size中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetFrameSize([out] unsigned long size);

    /**
     * @brief 获取音频buffer中的音频帧数。
     *
     * @param count 一个音频buffer中包含的音频帧数，获取后保存到count中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetFrameCount([out] unsigned long count);

    /**
     * @brief 设置音频采样的属性参数。
     *
     * @param attrs 待设置的音频采样属性，例如采样频率、采样精度、通道，详请参考{@link AudioSampleAttributes}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetSampleAttributes
     *
     * @since 4.1
     * @version 1.0
     */
    SetSampleAttributes([in] struct AudioSampleAttributes attrs);

    /**
     * @brief 获取音频采样的属性参数。
     *
     * @param attrs 获取的音频采样属性（例如采样频率、采样精度、通道）
     *              保存到attrs中，详请参考{@link AudioSampleAttributes}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SetSampleAttributes
     *
     * @since 4.1
     * @version 1.0
     */
    GetSampleAttributes([out] struct AudioSampleAttributes attrs);

    /**
     * @brief 获取音频的数据通道ID。
     *
     * @param channelId 获取的通道ID保存到channelId中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetCurrentChannelId([out] unsigned int channelId);

    /**
     * @brief 设置音频拓展参数。
     *
     * @param keyValueList 拓展参数键值对字符串列表，格式为key=value，多个键值对通过分号分割。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetExtraParams([in] String keyValueList);

    /**
     * @brief 获取音频拓展参数。
     *
     * @param keyValueList 拓展参数键值对字符串列表，格式为key=value，多个键值对通过分号分割。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetExtraParams([out] String keyValueList);
    /**
     * @brief 请求mmap缓冲区。
     *
     * @param reqSize 请求缓冲区的大小。
     * @param desc 缓冲区描述符，详请参考{@link AudioMmapBufferDescripter}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    ReqMmapBuffer([in] int reqSize, [out] struct AudioMmapBufferDescriptor desc);
    /**
     * @brief 获取当前mmap的读/写位置。
     *
     * @param frames 获取的音频帧计数保存到frames中。
     * @param time 获取的关联时间戳保存到time中，详请参考{@link AudioTimeStamp}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetMmapPosition([out] unsigned long frames, [out] struct AudioTimeStamp time);
    /**
     * @brief 添加音频效果。
     *
     * @param effectid 添加的音频效果实例标识符。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    AddAudioEffect([in] unsigned long effectid);
    /**
     * @brief 移除音频效果。
     *
     * @param effectid 移除的音频效果实例标识符。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    RemoveAudioEffect([in] unsigned long effectid);

    /**
     * @brief 获取缓冲区大小。
     *
     * @param bufferSize 获取的缓冲区大小保存在bufferSize中，单位为字节。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetFrameBufferSize([out] unsigned long bufferSize);
    /**
     * @brief 启动一个音频播放或录音处理。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see Stop
     *
     * @since 4.1
     * @version 1.0
     */
    Start();
    /**
     * @brief 停止一个音频播放或录音处理。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see Start
     *
     * @since 4.1
     * @version 1.0
     */
    Stop();

    /**
     * @brief 暂停一个音频播放或录音处理。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see Resume
     *
     * @since 4.1
     * @version 1.0
     */
    Pause();

    /**
     * @brief 恢复一个音频播放或录音处理。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see Pause
     *
     * @since 4.1
     * @version 1.0
     */
    Resume();

    /**
     * @brief 刷新音频缓冲区buffer中的数据。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    Flush();

    /**
     * @brief 设置或去设置设备的待机模式。
     *
     * @return 设置设备待机模式成功返回值0，失败返回负值；设置取消设备待机模式成功返回正值，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    TurnStandbyMode();
    /**
     * @brief Dump音频设备信息。
     *
     * @param range Dump信息范围，分为简要信息、全量信息。
     * @param fd 指定Dump目标文件。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    AudioDevDump([in] int range, [in] int fd);

    /**
     * @brief 判断声卡是否支持音频播放的暂停和恢复功能
     *
     * @param supportPause 是否支持暂停功能的状态保存到supportPause中，true表示支持，false表示不支持。
     * @param supportResume 是否支持恢复功能的状态保存到supportResume中，true表示支持，false表示不支持。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    IsSupportsPauseAndResume([out] boolean supportPause, [out] boolean supportResume);
    /**
     * @brief 设置低功耗模式缓存长度。
     *
     * @param size 包含音频数据的缓存长度。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
}
/** @} */