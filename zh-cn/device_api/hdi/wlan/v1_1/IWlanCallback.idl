/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @添加到组 WLAN
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点
 * WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file IWlanCallback.idl
 *
 * @brief 提供回调函数,当WLAN驱动重启、扫描结果返回时调用,接收到Netlink消息.
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @brief 定义WLAN模块接口的包路径.
 *
 * @since 3.2
 * @version 1.1
 */
package ohos.hdi.wlan.v1_1;

import ohos.hdi.wlan.v1_1.WlanTypes;

/**
 * @brief 定义WLAN模块的回调.
 *
 * 当WLAN模块重新启动、热点扫描结束,收到Netlink消息等情况下,调用该回调,继续后续处理,
 *
 * @since 3.2
 * @version 1.1
 */
[callback] interface IWlanCallback {
    /**
     * @brief 调用此方法,处理WLAN驱动重启时,返回的结果.
     *
     * @param event 表示驱动重启事件ID.
     * @param code 表示重启驱动时的返回结果.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @since 3.2
     * @version 1.1
     */
    ResetDriverResult([in] unsigned int event, [in] int code, [in] String ifName);

    /**
     * @brief 调用此方法,处理扫描结束时,返回的扫描结果.
     *
     * @param event 表示扫描结果事件的ID.
     * @param scanResult 表示扫描结果.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @since 3.2
     * @version 1.1
     */
    ScanResult([in] unsigned int event, [in] struct HdfWifiScanResult scanResult, [in] String ifName);

    /**
     * @brief 调用此方法以处理收到的Netlink消息.
     *
     * @param recvMsg 表示收到的Netlink消息.
     *
     * @since 3.2
     * @version 1.1
     */
    WifiNetlinkMessage([in] unsigned char[] recvMsg);

    /**
     * @brief 调用此方法,以处理扫描完成时,返回的扫描结果.
     *
     * @param event 表示扫描结果事件的ID.
     * @param scanResults 表示扫描结果(多个).
     * @param ifName 表示网卡(NIC)名称.
     *
     * @since 4.0
     * @version 1.1
     */
    ScanResults([in] unsigned int event, [in] struct HdfWifiScanResults scanResults, [in] String ifName);
}
/** @} */
