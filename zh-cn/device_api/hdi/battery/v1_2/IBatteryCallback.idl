/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup battery
 * @{
 *
 * @brief 提供获取和订阅电池信息的接口。
 *
 * 电池模块为电池服务提供的获取、订阅电池信息的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口获取、订阅电池信息。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file IBatteryCallback.idl
 *
 * @brief 提供电池信息的回调。
 *
 * 电池模块为电池服务提供回调，以便订阅电池信息的变更。
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.battery.v1_2;

import ohos.hdi.battery.v1_2.Types;

/**
 * @brief 表示电池信息的回调。
 *
 * 创建回调对象后，电池服务可调用 {@link IBatteryInterface} 接口注册回调，订阅电池信息变更。
 * 
 *
 * @since 3.1
 */
[callback] interface IBatteryCallback {

    /**
     * @brief 电池更改信息回调。
     *
     *  
     *
     * @param 输入参数，事件 电池信息，如电池电量、电压和健康状态。
     * @see BatteryInfo
     *
     * @since 3.1
     */
    Update([in] struct BatteryInfo event);
}
/** @} */
