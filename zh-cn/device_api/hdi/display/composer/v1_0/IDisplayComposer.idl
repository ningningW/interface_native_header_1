/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.0
 */

 /**
 * @file IDisplayComposer.idl
 *
 * @brief 显示合成接口声明。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Display模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.composer.v1_0;

import ohos.hdi.display.composer.v1_0.DisplayComposerType;
import ohos.hdi.display.composer.v1_0.IHotPlugCallback;
import ohos.hdi.display.composer.v1_0.IVBlankCallback;
import ohos.hdi.display.composer.v1_0.IRefreshCallback;

sequenceable OHOS.HDI.Display.HdifdParcelable;

 /**
 * @brief 显示合成接口声明。
 *
 * 主要提供注册热插拔事件回调、获取显示设备能力集等功能，具体方法使用详见函数说明。
 *
 * @since 3.2
 * @version 1.0
 */
interface IDisplayComposer {
    /**
     * @brief 注册热插拔事件回调。
     *
     * 注册热插拔事件回调，当有热插拔事件发生时接口实现层需要回调注册的接口，通过该实例通知图形服务。
     *
     * @param cb 热插拔事件回调实例。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    RegHotPlugCallback([in] IHotPlugCallback cb);

    /**
     * @brief 设置显示设备的客户端缓冲区缓存计数。
     *
     * @param devId 表示需要操作的设备ID。
     * @param count 客户端缓冲区缓存计数。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    SetClientBufferCacheCount([in] unsigned int devId, [in] unsigned int count);

     /**
     * @brief 注册VBlank事件回调。
     *
     * 注册VBlank事件回调，当有VBlank事件发生时接口实现层需要回调注册的接口。
     *
     * @param devId 表示需要操作的设备ID。
     * @param cb VBlank事件回调实例，当有VBlank事件发生时并且DisplayVsync处于Enable状态,接口实现层需要通过该实例通知图形服务。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    RegDisplayVBlankCallback([in] unsigned int devId, [in] IVBlankCallback cb);

    /**
     * @brief 获取显示设备能力集。
     *
     * 图形服务可以通过该接口获取显示设备具备哪些显示能力。
     *
     * @param devId 表示需要操作的设备ID。
     * @param info 设备支持的能力集信息，详情参考 {@link DisplayCapability}。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayCapability([in] unsigned int devId, [out] struct DisplayCapability info);

    /**
     * @brief 获取显示设备支持的显示模式信息。
     *
     * 图形服务可以通过该接口获取到显示设备支持哪些显示模式。
     *
     * @param devId 表示需要操作的设备ID。
     * @param modes 设备支持的所有模式信息，包括所有能支持的分辨率和刷新率，每一个模式实现层都有一个ID与之对应，在获取当前模式
     * 和设置当前模式时都会使用到，详情参考 {@link DisplayModeInfo}。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplaySupportedModes([in] unsigned int devId, [out] struct DisplayModeInfo[] modes);

    /**
     * @brief 获取显示设备当前的显示模式。
     *
     * 图形服务可以通过该接口获取显示设备当前的显示模式，该模式由接口实现层进行数据的写入。
     *
     * @param devId 表示需要操作的设备ID。
     * @param modeId 存放当前设备的显示模式ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayMode([in] unsigned int devId, [out] unsigned int modeId);

    /**
     * @brief 设置显示设备的显示模式。
     *
     * 图形服务可以通过该接口获取设置显示设备的显示模式。
     *
     * @param devId 表示需要操作的设备ID。
     * @param modeId 指明需要设置的模式ID，接口实现层将设备切换到该参数对应的显示模式。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayMode([in] unsigned int devId, [in] unsigned int modeId);

     /**
     * @brief 获取显示设备当前的电源状态。
     *
     * 图形服务可以通过该接口获取设置显示设备的电源状态，该电源状态由接口实现层进行状态的写入。
     *
     * @param devId 表示需要操作的设备ID。
     * @param status 保存对应设备的电源状态，具体电源状态查看{@link DispPowerStatus}。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayPowerStatus([in] unsigned int devId, [out] enum DispPowerStatus status);

    /**
     * @brief 设置显示设备当前的电源状态。
     *
     * 图形服务可以通过该接口获取设置显示设备的电源状态。
     *
     * @param devId 表示需要操作的设备ID。
     * @param status 表示需要设置的电源状态，具体电源状态查看{@link DispPowerStatus}。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayPowerStatus([in] unsigned int devId, [in] enum DispPowerStatus status);

    /**
     * @brief 获取显示设备当前的背光值。
     *
     * 图形服务可以通过该接口获取设置显示设备的背光值。
     *
     * @param devId 表示需要操作的设备ID。
     * @param level 保存对应设备的背光值，由接口实现层进行写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayBacklight([in] unsigned int devId, [out] unsigned int level);

    /**
     * @brief 设置显示设备当前的背光值。
     *
     * 图形服务可以通过该接口获取设置显示设备的背光值。
     *
     * @param devId 表示需要操作的设备ID。
     * @param level 表示需要设置的背光值，背光值范围0~255。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayBacklight([in] unsigned int devId, [in] unsigned int level);

    /**
     * @brief 使能垂直同步信号。
     *
     * 图形服务可以通过该接口使能或取消垂直同步信号，当有垂直同步信号产生时，接口实现层需要回调图形服务通过RegDisplayVBlankCallback注册的
     * VBlankCallback 回调。
     * 图形服务在需要刷新显示时需要使能垂直同步信号，在收到{@link VBlankCallback}事件回调时再进行合成送显,不需要刷新显示时需要取消垂直同步信号。
     * @param devId 表示需要操作的设备ID。
     * @param enabled 使能状态，true表示能，false表示不能。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayVsyncEnabled([in] unsigned int devId, [in] boolean enabled);

    /**
     * @brief 打开图层。
     *
     * GUI在使用图层时，需要先根据图层信息打开图层，打开图层成功可获得图层ID，根据图层ID使用图层各接口。
     *
     * @param devId 显示设备ID，用于支持多个显示设备，取值从0开始，0表示第一个设备，最大支持5个设备，即取值范围0~4。
     * @param layerInfo 图层信息，上层GUI打开图层时需传递图层信息，包括图层类型，图层大小，像素格式等信息。
     * @param layerId 图层ID，打开图层成功后返回给GUI的图层ID，用于标识唯一的图层。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @see CloseLayer
     * @since 3.2
     * @version 1.0
     */
    CreateLayer([in] unsigned int devId, [in] struct LayerInfo layerInfo, [in] unsigned int cacheCount,
        [out] unsigned int layerId);

    /**
     * @brief 在指定的显示设备上打开图层。
     *
     * 在 GUI 上使用图层之前，必须根据图层信息打开图层。在图层
     * 打开后，可以获取图层 ID，然后根据图层 ID 使用其他功能。 
     *
     * @param devId：显示设备的ID。取值范围为 0 到 4，其中 0 表示第一个显示设备，4 表示最后一个显示设备。
     * @param layerId 指示指向唯一标识层的层 ID 的指针。返回图层 ID到图层成功打开后添加到 GUI。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    DestroyLayer([in] unsigned int devId, [in] unsigned int layerId);

    /**
     * @brief 设置显示设备的裁剪区域。
     *
     * 图形服务可以通过该接口设置显示设备的ClientBuffer的裁剪区域，裁剪区域不能超过ClientBuffer的大小。
     *
     * @param devId 表示需要操作的设备ID。
     * @param rect ClientBuffer的裁剪区域。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayClientCrop([in] unsigned int devId, [in] struct IRect rect);

    /**
     * @brief 获取显示图层fence。
     *
     * 图形服务在调用接口Commit后，需要通过该接口获取图层的fence信息。
     *
     * @param devId 表示需要操作的设备ID。
     * @param layers 图层首地址，指向图层数组的首地址。
     * @param fences fence首地址，指向fence数组的首地址。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayReleaseFence([in] unsigned int devId, [out] unsigned int[] layers, [out] HdifdParcelable[] fences);

    /**
     * @brief 创建虚拟显示设备。
     *
     * 该接口用于创建一个虚拟显示设备。
     *
     * @param width 指定显示设备的像素宽度。
     * @param height 指定显示设备的像素高度。
     * @param format 指定显示设备的像素格式。
     * 详情参考{@link PixelFormat}，接口实现层可以根据硬件需求，修改format并返回给图形服务。
     * @param devId 用于接口层返回创建的设备ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    CreateVirtualDisplay([in] unsigned int width, [in] unsigned int height, [out] int format, [out] unsigned int devId);

    /**
     * @brief 销毁虚拟显示设备。
     *
     * 该接口用于销毁指定的虚拟显示设备。
     *
     * @param devId 表示需要操作的设备ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    DestroyVirtualDisplay([in] unsigned int devId);

    /**
     * @brief 设置虚拟屏的输出缓存。
     *
     * 该接口用于设置虚拟屏输出缓存，接口实现层需要将虚拟屏的输出放入到该缓存中，接口实现层需要等待同步栅栏发送信号后才能使用缓存。
     *
     * @param devId 表示需要操作的设备ID。
     * @param buffer 输出缓存。
     * @param fence 同步栅栏。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetVirtualDisplayBuffer([in] unsigned int devId, [in] NativeBuffer buffer, [in] HdifdParcelable fence);

    /**
     * @brief 设置显示设备属性值。
     *
     * 图形服务可以通过该接口设置显示设备具体的属性值。
     *
     * @param devId 表示需要操作的设备ID。
     * @param id 由接口{@link GetDisplayCapability}返回属性ID。
     * @param value 需要设置的属性值。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetDisplayProperty([in] unsigned int devId, [in] unsigned int id, [in] unsigned long value);

    /**
     * @brief 获取显示设备属性值。
     *
     * 图形服务可以通过该接口获取显示设备具体的属性值。
     *
     * @param devId 指示需要操作的设备ID。
     * @param id 由接口{@Link GetDisplayCapability}返回的属性ID。
     * @param value 属性ID对应的属性值，由接口实现层写入。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDisplayProperty([in] unsigned int devId, [in] unsigned int id, [out] unsigned long value);

    /* 用于 SMQ 传输的 FUNC */
    /* *
     * @brief 初始化命令请求对象。
     *
     * @param request 指示要初始化的 SharedMemQueue。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    InitCmdRequest([in] SharedMemQueue<int> request);

    /* *
     * @brief 发送命令请求。
     *
     * @param inEleCnt 表示元素的个数。
     * @param inFds 表示 HdifdParcelable 的 ID。
     * @param outEleCnt outEleCnt inEleCnt 指示要获取的元素数。
     * @param outFds outEleCnt 指示要获取的 HdifdParcelable 的 ID。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    CmdRequest([in] unsigned int inEleCnt, [in] struct HdifdInfo[] inFds, [out] unsigned int outEleCnt,
        [out] struct HdifdInfo[] outFds);

    /* *
     * @brief 获取命令请求的返回结果。
     *
     * @param reply 表示返回的结果。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    GetCmdReply([out] SharedMemQueue<int> reply);
}
/** @} */