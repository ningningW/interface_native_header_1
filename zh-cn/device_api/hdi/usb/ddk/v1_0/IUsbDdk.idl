/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiUsbDdk
 * @{
 *
 * @brief 提供USB DDK API以打开和关闭USB接口，执行非等时和等时。
 *
 * 通过USB管道进行数据传输，并实现控制传输和中断传输等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IUsbDdk.idl
 *
 * @brief 声明USB主机用于访问USB设备的USB DDK API。
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.usb.ddk.v1_0;

import ohos.hdi.usb.ddk.v1_0.UsbDdkTypes;

/**
 * @brief 声明USB主机用于访问USB设备的USB DDK API。
 *
 * 上层USB服务调用相关功能接口，可以打开/关闭设备，获取设备描述符，批量读取/写入数据等。
 *
 * @since 4.0
 * @version 1.0
 */
interface IUsbDdk
{

    /**
     * @brief 初始化DDK。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    Init();   

     /**
     * @brief 在停止数据传输后关闭占用的USB设备接口，并释放相关资源。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    Release();

    /**
     * @brief 获取USB设备描述符。
     *
     * @param deviceId 要获取其描述符的设备的设备Id。
     * @param desc USB协议中定义的标准设备描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    GetDeviceDescriptor([in] unsigned long deviceId, [out] struct UsbDeviceDescriptor desc);

    /**
     * @brief 获取配置描述符。
     *
     * @param deviceId 要获取其描述符的设备的设备Id。
     * @param configIndex 配置索引，对应于USB协议中的<b>bConfigurationValue</b>。
     * @param configDesc 配置描述符，包括USB协议中定义的标准配置描述符以及相关的接口描述符和端点描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    GetConfigDescriptor([in] unsigned long deviceId, [in] unsigned char configIndex, [out] List<unsigned char> configDesc);

    /**
     * @brief 声明USB接口。
     *
     * @param deviceId 要操作的设备的ID。
     * @param interfaceIndex 接口索引，对应于USB协议中的<b>b接口编号</b>。
     * @param interfaceHandle 接口操作处理。成功声明接口后，将为此参数分配一个值。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    ClaimInterface([in] unsigned long deviceId, [in] unsigned char interfaceIndex, [out] unsigned long interfaceHandle);

    /**
     * @brief 在停止数据传输后关闭占用的USB设备接口，并释放相关资源。
     *
     * @param interfaceHandle 接口操作处理。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    ReleaseInterface([in] unsigned long interfaceHandle);

    /**
     * @brief 激活USB接口的备用设置。
     *
     * @param interfaceHandle 接口操作处理。
     * @param settingIndex 备用设置的索引，对应于USB协议中的<b>bAlternateSetting</b>。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    SelectInterfaceSetting([in] unsigned long interfaceHandle, [in] unsigned char settingIndex);

    /**
     * @brief 获取USB接口的激活备用设置。
     *
     * @param interfaceHandle 接口操作处理。
     * @param settingIndex 备用设置的索引，对应于USB协议中的<b>bAlternateSetting</b>。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    GetCurrentInterfaceSetting([in] unsigned long interfaceHandle, [out] unsigned char settingIndex);

    /**
     * @brief 发送控制读取传输请求。此API以同步方式工作。
     *
     * @param interfaceHandle 接口操作处理。
     * @param setup 请求数据，对应于USB协议中的<b>Setup Data</b>。
     * @param timeout 超时持续时间，以毫秒为单位。
     * @param data 要传输的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    SendControlReadRequest([in] unsigned long interfaceHandle, [in] struct UsbControlRequestSetup setup, [in] unsigned int timeout, [out] List<unsigned char> data);

    /**
     * @brief 发送控制写入传输请求。此API以同步方式工作。
     *
     * @param interfaceHandle 接口操作处理。
     * @param setup 请求数据，对应于USB协议中的<b>Setup Data</b>。
     * @param timeout 超时持续时间，以毫秒为单位。
     * @param data 要传输的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    SendControlWriteRequest([in] unsigned long interfaceHandle, [in] struct UsbControlRequestSetup setup, [in] unsigned int timeout, [in] List<unsigned char> data);

    /**
     * @brief 发送管道请求。此API以同步方式工作。此API适用于中断传输和批量传输。
     *
     * @param 管道用于传输数据的管道。
     * @param size 缓冲区大小。
     * @param offset 所用缓冲区的偏移量。默认值为0，表示没有偏移，缓冲区从指定地址开始。
     * @param length 使用的缓冲区的长度。默认情况下，该值等于大小，表示使用了整个缓冲区。
     * @param transferedLength 传输数据的长度。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    SendPipeRequest([in] struct UsbRequestPipe pipe, [in] unsigned int size, [in] unsigned int offset, [in] unsigned int length, [out] unsigned int transferedLength);

    /**
     * @brief 获取内存映射的文件描述符。
     *
     * @param deviceId 待操作设备的ID。
     * @param fd 为内存映射获取的文件描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 4.0
     * @version 1.0
     */
    GetDeviceMemMapFd([in] unsigned long deviceId, [out] FileDescriptor fd);
}；
/** @} */
