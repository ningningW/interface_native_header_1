/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiNfc
 * @{
 *
 * @brief 为nfc服务提供统一的访问nfc驱动的接口
 *
 * NFC服务通过获取的nfc驱动对象提供的API接口访问nfc驱动，包括开关NFC、初始化NFC、读写数据、配置RF参数、
 * 通过IO控制发送NCI指令给nfc驱动
 *
 * @since 4.1
 * @version 1.1
 */

/**
 * @file NfcTypes.idl
 *
 * @brief NFC事件（包括打开NFC完成、关闭NFC完成、预配置NFC完成等上报事件）的枚举定义。
 *
 * @since 4.1
 * @version 1.1
 */

package ohos.hdi.nfc.v1_1;

/**
 * @brief RF discover过程中厂家自定义支持的NFC协议类型。
 *
 * @since 4.1
 */
enum VendorProtocalDiscoveryCfg {
    /** 遵循ISO18092协议的Felica */
    NCI_PROTOCOL_18092_ACTIVE         = 0,
    /** 遵循typeB类型的ISO-DEP */
    NCI_PROTOCOL_B_PRIME              = 1,
    /** 厂商自定义的私有协议 */
    NCI_PROTOCOL_DUAL                 = 2,
    /** 遵循ISO15693协议 */
    NCI_PROTOCOL_15693                = 3,
    /** Kovio公司的NFC条形码 */
    NCI_PROTOCOL_KOVIO                = 4,
    /** 厂商定义的Mifare协议 */
    NCI_PROTOCOL_MIFARE               = 5,
    /** Kovio的polling */
    NCI_DISCOVERY_TYPE_POLL_KOVIO     = 6,
    /** typeB的polling */
    NCI_DISCOVERY_TYPE_POLL_B_PRIME   = 7,
    /** typeB的卡模拟 */
    NCI_DISCOVERY_TYPE_LISTEN_B_PRIME = 8,
    /** 自定义最大配置个数 */
    VENDOR_PROPEIETARY_CFG_MAX        = 9,
};

/**
 * @brief 厂家自定义的NFC配置。
 *
 * @since 4.1
 */
struct NfcVendorConfig {
    /** 为ISO_DEP定义的扩展APDU长度 */
    unsigned int isoDepExtApduLength;

    /** 默认路由的SE ID */
    unsigned char defaultOffHostRoute;

    /** 基于Felica的默认路由SE ID */
    unsigned char defaultOffHostRouteFelica;

    /** 基于Felica协议中定义的system code路由的默认路由位置 */
    unsigned char defaultSysCodeRoute;

    /** 基于Felica协议中定义的system code路由支持的电源状态 */
    unsigned char defaultSysCodePwrState;

    /** 基于协议和技术的默认路由. */
    unsigned char defaultUnconfiguredRoute;

    /** 配置ese使用的pipeID */
    unsigned char esePipeId;

    /** 配置SIM卡使用的pipeID */
    unsigned char simPipeId;

    /** 配置是否支持bai-out模式，为TRUE则支持，否则则是typeA/B轮询. */
    boolean pollBailOutMode;

    /** 为T4T卡选择卡在位检测算法.若未定义则默认使用I_BLOCK. */
    unsigned char checkAlgorithm;

    /** 厂家自定义的私有协议和探索配置. */
    unsigned char []vendorProtocalDiscoveryCfg;

    /** 厂家自定义的私有协议和探索配置数据大小. */
    unsigned char vendorProtocalDiscoveryCfgSize;

    /** 主机白名单 */
    List<unsigned char> hostWhitelist;

    /** 多SE场景，选择默认路由SIM的offhost列表 */
    List<unsigned char> offHostRouteUicc;

    /** 多SE场景，选择默认路由eSE的offhost列表 */
    List<unsigned char> offHostRouteEse;

    /** ISO-DEP默认路由位置 */
    unsigned char defaultIsoDepRoute;
};
/** @} */