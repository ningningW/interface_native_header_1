/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Motion
 * @{
 *
 * @brief 手势识别设备驱动对硬件服务提供通用的接口能力。
 *
 * 模块提供硬件服务对手势识别驱动模块访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现使能手势识别/
 * 去使能手势识别、订阅/取消订阅手势识别数据。
 *
 * @since 4.0
 */

/**
 * @file IMotionInterface.idl
 *
 * @brief 定义使能/去使能手势识别、订阅/取消订阅手势识别数据的接口。
 *
 * 在实现拿起、翻转、摇一摇、旋转屏等手势识别功能时，需要调用此处定义的接口。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @brief 手势识别模块接口的包路径。
 *
 * @since 4.0
 */
package ohos.hdi.motion.v1_1;

import ohos.hdi.motion.v1_0.IMotionCallback;
import ohos.hdi.motion.v1_0.IMotionInterface;
import ohos.hdi.motion.v1_1.MotionTypes;

/**
 * @brief 提供Motion设备基本控制操作接口。
 *
 * 接口提供使能/去使能手势识别、订阅/取消订阅手势识别数据功能。
 */
interface IMotionInterface extends ohos.hdi.motion.v1_0.IMotionInterface {
    /**
     * @brief 设置运动配置信息。
     *
     * @param motionType 运动类型。有关详细信息，请参阅｛@link HdfMotionTypeTag｝。
     * @param data 一个技巧的运动波配置参数。有关详细信息，请参阅｛@link WaveParam｝。
     * @param len 数据长度。
     * 
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.1
     */
    SetMotionConfig([in] int motionType, [in] unsigned char[] data);
}
/** @} */