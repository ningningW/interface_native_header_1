/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file native_audiorenderer.h
 *
 * @brief 声明输出类型的音频流相关接口,
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 * @version 1.0
 */

#ifndef NATIVE_AUDIORENDERER_H
#define NATIVE_AUDIORENDERER_H

#include <time.h>
#include "native_audiostream_base.h"
#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 释放音频流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_Release(OH_AudioRenderer* renderer);

/**
 * @brief 开始输出音频数据。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_Start(OH_AudioRenderer* renderer);

/**
 * @brief 暂停音频流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_Pause(OH_AudioRenderer* renderer);

/**
 * @brief 停止音频流
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_Stop(OH_AudioRenderer* renderer);

/**
 * @brief 丢弃已经写入的音频数据。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_Flush(OH_AudioRenderer* renderer);

/**
 * @brief 查询当前音频流状态。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param state 指向一个用来接收音频流状态的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetCurrentState(OH_AudioRenderer* renderer,
    OH_AudioStream_State* state);

/**
 * @brief 查询当前音频流采样率。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param rate 指向一个用来接收音频流采样率的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSamplingRate(OH_AudioRenderer* renderer, int32_t* rate);

/**
 * @brief 查询当前音频流ID。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param streamId 指向一个用来接收音频流ID的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetStreamId(OH_AudioRenderer* renderer, uint32_t* streamId);

/**
 * @brief 查询当前音频流通道数。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param channelCount 指向一个用来接收音频流通道数的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetChannelCount(OH_AudioRenderer* renderer, int32_t* channelCount);

/**
 * @brief 查询当前音频流采样格式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param sampleFormat 指向一个用来接收音频流采样格式的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSampleFormat(OH_AudioRenderer* renderer,
    OH_AudioStream_SampleFormat* sampleFormat);

/**
 * @brief 查询当前音频流时延模式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param latencyMode 指向一个用来接收音频流时延模式的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetLatencyMode(OH_AudioRenderer* renderer,
    OH_AudioStream_LatencyMode* latencyMode);

/**
 * @brief 查询当前音频流工作场景类型。
 *
 * The rendere info includes {@link OH_AudioStream_Usage} value and {@link OH_AudioStream_Content} value.
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param usage 指向一个用来接收输出类型音频流的工作场景的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetRendererInfo(OH_AudioRenderer* renderer,
    OH_AudioStream_Usage* usage);

/**
 * @brief 查询当前音频流编码类型。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param encodingType 指向一个用来接收音频流编码类型的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetEncodingType(OH_AudioRenderer* renderer,
    OH_AudioStream_EncodingType* encodingType);

/**
 * @brief 查询自创建流以来已写入的帧数。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param frames 指向将为帧计数设置的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetFramesWritten(OH_AudioRenderer* renderer, int64_t* frames);

/**
 * @brief 获取输出音频流时间戳和位置信息。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param clockId 时钟标识符，使用：{@link #CLOCK_MONOTONIC}。
 * @param framePosition 指向要接收位置的变量的指针(作为返回值使用)。
 * @param timestamp 指向接收时间戳的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetTimestamp(OH_AudioRenderer* renderer, clockid_t clockId,
    int64_t* framePosition, int64_t* timestamp);

/**
 * @brief 在回调中查询帧大小，它是一个固定的长度，每次回调都要填充流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param frameSize 指向将为帧大小设置的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetFrameSizeInCallback(OH_AudioRenderer* renderer, int32_t* frameSize);

/**
 * @brief 获取音频渲染速率。
 *
 * @since 11
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param speed 指向接收播放倍速值的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSpeed(OH_AudioRenderer* renderer, float* speed);

/**
 * @brief 设置音频渲染速率。
 *
 * @since 11
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param speed 设置播放的倍速值（倍速范围：0.25-4.0）。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioRenderer_SetSpeed(OH_AudioRenderer* renderer, float speed);
#ifdef __cplusplus
}
#endif
#endif // NATIVE_AUDIORENDERER_H
/** @} */