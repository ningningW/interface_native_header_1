/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file native_audiocapturer.h
 *
 * @brief 声明输入类型的音频流相关接口,
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 * @version 1.0
 */

#ifndef NATIVE_AUDIOCAPTURER_H
#define NATIVE_AUDIOCAPTURER_H

#include <time.h>
#include "native_audiostream_base.h"
#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 释放音频流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @permission ohos.permission.MICROPHONE
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_Release(OH_AudioCapturer* capturer);

/**
 * @brief 开始获取音频数据。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @permission ohos.permission.MICROPHONE
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_Start(OH_AudioCapturer* capturer);

/**
 * @brief 暂停音频流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @permission ohos.permission.MICROPHONE
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_Pause(OH_AudioCapturer* capturer);

/**
 * @brief 停止音频流
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @permission ohos.permission.MICROPHONE
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_Stop(OH_AudioCapturer* capturer);

/**
 * @brief 丢弃获取的音频数据。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_Flush(OH_AudioCapturer* capturer);

/**
 * @brief 查询当前音频流状态。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param state 指向一个用来接收音频流状态的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetCurrentState(OH_AudioCapturer* capturer, OH_AudioStream_State* state);

/**
 * @brief 查询当前音频流时延模式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param latencyMode 指向一个用来接收音频流时延模式的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetLatencyMode(OH_AudioCapturer* capturer,
    OH_AudioStream_LatencyMode* latencyMode);

/**
 * @brief 查询当前音频流ID。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param streamId 指向一个用来接收音频流ID的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetStreamId(OH_AudioCapturer* capturer, uint32_t* streamId);

/**
 * @brief 查询当前音频流采样率。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param rate 指向一个用来接收音频流采样率的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetSamplingRate(OH_AudioCapturer* capturer, int32_t* rate);

/**
 * @brief 查询当前音频流通道数。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param channelCount 指向一个用来接收音频流通道数的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetChannelCount(OH_AudioCapturer* capturer, int32_t* channelCount);

/**
 * @brief 查询当前音频流采样格式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param sampleFormat 指向一个用来接收音频流采样格式的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetSampleFormat(OH_AudioCapturer* capturer,
    OH_AudioStream_SampleFormat* sampleFormat);

/**
 * @brief 查询当前音频流编码类型。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param encodingType 指向一个用来接收音频流编码类型的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetEncodingType(OH_AudioCapturer* capturer,
    OH_AudioStream_EncodingType* encodingType);

/**
 * @brief 查询当前音频流工作场景类型。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param sourceType 指向一个用来接收输入类型音频流的工作场景的变量(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetCapturerInfo(OH_AudioCapturer* capturer,
    OH_AudioStream_SourceType* sourceType);

/**
 * @brief 在回调中查询帧大小，它是每次回调返回的缓冲区的固定长度。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param frameSize 指向将为帧大小设置的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetFrameSizeInCallback(OH_AudioCapturer* capturer,
    int32_t* frameSize);

/**
 * @brief 获取输入音频流时间戳和位置信息。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param clockId 时钟标识符，使用：{@link #CLOCK_MONOTONIC}。
 * @param framePosition 指向要接收位置的变量的指针(作为返回值使用)。
 * @param timestamp 指向接收时间戳的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetTimestamp(OH_AudioCapturer* capturer, clockid_t clockId,
    int64_t* framePosition, int64_t* timestamp);

/**
 * @brief 查询自创建流以来已读取的帧数。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
 * @param frames 指向将为帧计数设置的变量的指针(作为返回值使用)。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioCapturer_GetFramesRead(OH_AudioCapturer* capturer, int64_t* frames);
#ifdef __cplusplus
}
#endif

#endif // NATIVE_AUDIOCAPTURER_H
/** @} */