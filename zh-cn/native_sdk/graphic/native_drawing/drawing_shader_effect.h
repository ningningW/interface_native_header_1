/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_SHADER_EFFECT_H
#define C_INCLUDE_DRAWING_SHADER_EFFECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_shader_effect.h
 *
 * @brief 声明与绘图模块中的着色器对象相关的函数。
 *
 * 引用文件"native_drawing/drawing_shader_effect.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 着色器效果平铺模式的枚举。
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_TileMode {
    /**
     * 如果着色器效果超出其原始边界，则复制边缘颜色。
     */
    CLAMP,
    /**
     * 在水平和垂直方向上重复着色器效果图像。
     */
    REPEAT,
    /**
     * 水平和垂直重复着色器效果图像，交替镜像。
     */
    MIRROR,
    /**
     * 只在原始区域内绘制，其他地方返回透明黑色。
     */
    DECAL,
} OH_Drawing_TileMode;

/**
 * @brief 创建着色器，在两个指定点之间生成线性渐变。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param startPt 表示渐变的起点。
 * @param endPt 表示渐变的终点。
 * @param colors 表示在两个点之间分布的颜色。
 * @param pos 表示每种对应颜色在颜色数组中的相对位置。
 * @param size 表示颜色和位置的数量。
 * @param OH_Drawing_TileMode 着色器效果平铺模式类型，支持可选的具体模式可见{@Link OH_Drawing_TileMoe}枚举。
 * @return 返回创建的着色器对象的指针。
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateLinearGradient(const OH_Drawing_Point* startPt,
    const OH_Drawing_Point* endPt, const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief 创建着色器，在给定圆心和半径的情况下生成径向渐变。
 * 从起点到终点颜色从内到外进行圆形渐变（从中间向外拉）被称为径向渐变。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt 指示渐变的圆心。
 * @param radius 指示此渐变的圆半径。
 * @param colors 表示在两个点之间分布的颜色。
 * @param pos 表示每种对应颜色在颜色数组中的相对位置。
 * @param size 表示颜色和位置的数量。
 * @param OH_Drawing_TileMode 着色器效果平铺模式类型，支持可选的具体模式可见{@Link OH_Drawing_TileMoe}枚举。
 * @return 返回创建的着色器对象的指针。
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateRadialGradient(const OH_Drawing_Point* centerPt, float radius,
    const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief 创建着色器，在给定中心的情况下生成扇形渐变。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt 表示渐变的圆心。
 * @param colors 表示在两个点之间分布的颜色。
 * @param pos 表示每种对应颜色在颜色数组中的相对位置。
 * @param size 表示颜色和位置的数量。
 * @param OH_Drawing_TileMode 着色器效果平铺模式类型，支持可选的具体模式可见{@Link OH_Drawing_TileMoe}枚举。
 * @return 返回创建的着色器对象的指针。
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateSweepGradient(const OH_Drawing_Point* centerPt,
    const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief 销毁着色器对象，并收回该对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ShaderEffect 表示指向着色器对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_ShaderEffectDestroy(OH_Drawing_ShaderEffect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif