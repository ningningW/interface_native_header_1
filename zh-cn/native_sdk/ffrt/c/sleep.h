/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file sleep.h
 *
 * @brief 声明sleep和yield C接口.
 *
 * @since 10
 * @version 1.0
 */
#ifndef FFRT_API_C_SLEEP_H
#define FFRT_API_C_SLEEP_H
#include "type_def.h"

/**
 * @brief 延迟usec微秒.
 *
 * @param usec延迟时间，单位微秒.
 * @return 执行成功时返回ffrt_thrd_success,
           执行成功时返回ffrt_thrd_error.
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_usleep(uint64_t usec);

/**
 * @brief 当前任务主动放权，让其他任务有机会调度执行.
 *
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_yield(void);
/** @} */
#endif
