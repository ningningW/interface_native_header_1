/*
 * Copyright (c) 2013-2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ----------------------------------------------------------------------
 *
 * $Date:        12. June 2020
 * $Revision:    V2.1.3
 *
 * Project:      CMSIS-RTOS2 API
 * Title:        cmsis_os2.h header file
 *
 * Version 2.1.3
 *    Additional functions allowed to be called from Interrupt Service Routines:
 *    - osThreadGetId
 * Version 2.1.2
 *    Additional functions allowed to be called from Interrupt Service Routines:
 *    - osKernelGetInfo, osKernelGetState
 * Version 2.1.1
 *    Additional functions allowed to be called from Interrupt Service Routines:
 *    - osKernelGetTickCount, osKernelGetTickFreq
 *    Changed Kernel Tick type to uint32_t:
 *    - updated: osKernelGetTickCount, osDelayUntil
 * Version 2.1.0
 *    Support for critical and uncritical sections (nesting safe):
 *    - updated: osKernelLock, osKernelUnlock
 *    - added: osKernelRestoreLock
 *    Updated Thread and Event Flags:
 *    - changed flags parameter and return type from int32_t to uint32_t
 * Version 2.0.0
 *    Initial Release
 *---------------------------------------------------------------------------*/

/**
* @addtogroup CMSIS-RTOS
* @{
*
* @brief Provides standard Common Microcontroller Software Interface Standard (CMSIS) 2.0 APIs for devices powered by Arm Cortex-M processors.
*
* You can use the APIs for the software components that run the real-time operating system (RTOS).
*
* @since 1.0
*/

/**
* @file cmsis_os2.h
*
* @brief Defines standard CMSIS 2.0 APIs.
*
* This header file provides standard CMSIS 2.0 APIs, including the APIs for the management of the kernel, threads, timers, events,
* memory, mutexes, message queues and semaphores, and definitions of related structures and macros. \n
*
*
* @since 1.0
*/
#ifndef CMSIS_OS2_H_
#define CMSIS_OS2_H_

#ifndef __NO_RETURN
#if   defined(__CC_ARM)
#define __NO_RETURN __declspec(noreturn)
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
#define __NO_RETURN __attribute__((__noreturn__))
#elif defined(__GNUC__)
#define __NO_RETURN __attribute__((__noreturn__))
#elif defined(__ICCARM__)
#define __NO_RETURN __noreturn
#else
#define __NO_RETURN
#endif
#endif

#include <stdint.h>
#include <stddef.h>

#ifdef  __cplusplus
extern "C"
{
#endif

/**
* @brief Defines the system version information.
*
* 
*
* @since 1.0
*/
typedef struct {
  /** API version. */
  uint32_t                       api;
  /** Kernel version. */
  uint32_t                    kernel;
} osVersion_t;

/**
* @brief Enumerates the kernel states.
*
* 
*
* @since 1.0
*/
typedef enum {
  /** Inactive. */
  osKernelInactive        =  0,
  /** Ready. */
  osKernelReady           =  1,
  /** Running. */
  osKernelRunning         =  2,
  /** Locked. */
  osKernelLocked          =  3,
  /** Suspended. */
  osKernelSuspended       =  4,
  /** Error. */
  osKernelError           = -1,
  /** Value reserved to prevent from enum down-size compiler optimization. */
  osKernelReserved        = 0x7FFFFFFF
} osKernelState_t;

/**
* @brief Enumerates the thread states.
*
* 
*
* @since 1.0
*/
typedef enum {
  /** Inactive. */
  osThreadInactive        =  0,
  /** Ready. */
  osThreadReady           =  1,
  /** Running. */
  osThreadRunning         =  2,
  /** Blocked. */
  osThreadBlocked         =  3,
  /** Terminated. */
  osThreadTerminated      =  4,
  /** Error. */
  osThreadError           = -1,
  /** Value reserved to prevent from enum down-size compiler optimization. */
  osThreadReserved        = 0x7FFFFFFF
} osThreadState_t;

/**
* @brief Enumerates the thread priorities.
*
*
*
* @since 1.0
*/
typedef enum {
  /** No priority (not initialized). */
  osPriorityNone          =  0,
  /** Priority for idle threads. This priority cannot be used for non-idle threads. */
  osPriorityIdle          =  1,
  /** Low. This is the lowest priority for non-idle threads. */
  osPriorityLow           =  8,
  /** Low + 1. */
  osPriorityLow1          =  8+1,
  /** Low + 2. */
  osPriorityLow2          =  8+2,
  /** Low + 3. */
  osPriorityLow3          =  8+3,
  /** Low + 4. */
  osPriorityLow4          =  8+4,
  /** Low + 5. */
  osPriorityLow5          =  8+5,
  /** Low + 6. */
  osPriorityLow6          =  8+6,
  /** Low + 7. */
  osPriorityLow7          =  8+7,
  /** Below normal. */
  osPriorityBelowNormal   = 16,
  /** Below normal + 1. */
  osPriorityBelowNormal1  = 16+1,
  /** Below normal + 2. */
  osPriorityBelowNormal2  = 16+2,
  /** Below normal + 3. */
  osPriorityBelowNormal3  = 16+3,
  /** Below normal + 4. */
  osPriorityBelowNormal4  = 16+4,
  /** Below normal + 5. */
  osPriorityBelowNormal5  = 16+5,
  /** Below normal + 6. */
  osPriorityBelowNormal6  = 16+6,
  /** Below normal + 7. */
  osPriorityBelowNormal7  = 16+7,
  /** Normal. */
  osPriorityNormal        = 24,
  /** Normal + 1. */
  osPriorityNormal1       = 24+1,
  /** Normal + 2. */
  osPriorityNormal2       = 24+2,
  /** Normal + 3. */
  osPriorityNormal3       = 24+3,
  /** Normal + 4. */
  osPriorityNormal4       = 24+4,
  /** Normal + 5. */
  osPriorityNormal5       = 24+5,
  /** Normal + 6. */
  osPriorityNormal6       = 24+6,
  /** Normal + 7. */
  osPriorityNormal7       = 24+7,
  /** Above normal. */
  osPriorityAboveNormal   = 32,
  /** Above normal + 1. This is the highest priority supported in the current version. */
  osPriorityAboveNormal1  = 32+1,
  /** Above normal + 2. */
  osPriorityAboveNormal2  = 32+2,
  /** Above normal + 3. */
  osPriorityAboveNormal3  = 32+3,
  /** Above normal + 4. */
  osPriorityAboveNormal4  = 32+4,
  /** Above normal + 5. */
  osPriorityAboveNormal5  = 32+5,
  /** Above normal + 6. */
  osPriorityAboveNormal6  = 32+6,
  /** Above normal + 7. */
  osPriorityAboveNormal7  = 32+7,
  /** High. */
  osPriorityHigh          = 40,
  /** High + 1. */
  osPriorityHigh1         = 40+1,
  /** High + 2. */
  osPriorityHigh2         = 40+2,
  /** High + 3. */
  osPriorityHigh3         = 40+3,
  /** High + 4. */
  osPriorityHigh4         = 40+4,
  /** High + 5. */
  osPriorityHigh5         = 40+5,
  /** High + 6. */
  osPriorityHigh6         = 40+6,
  /** High + 7. */
  osPriorityHigh7         = 40+7,
  /** Realtime. */
  osPriorityRealtime      = 48,
  /** Realtime + 1. */
  osPriorityRealtime1     = 48+1,
  /** Realtime + 2. */
  osPriorityRealtime2     = 48+2,
  /** Realtime + 3. */
  osPriorityRealtime3     = 48+3,
  /** Realtime + 4. */
  osPriorityRealtime4     = 48+4,
  /** Realtime + 5. */
  osPriorityRealtime5     = 48+5,
  /** Realtime + 6. */
  osPriorityRealtime6     = 48+6,
  /** Realtime + 7. */
  osPriorityRealtime7     = 48+7,
  /** Priority reserved for the interrupt service routine (ISR) deferred thread. */
  osPriorityISR           = 56,
  /** Invalid priority. */
  osPriorityError         = -1,
  /** Value reserved to prevent from enum down-size compiler optimization. */
  osPriorityReserved      = 0x7FFFFFFF
} osPriority_t;

/**
* @brief Defines the entry function for threads.
*
* This function is used as a callback during thread scheduling.
*
* @since 1.0
*/
typedef void (*osThreadFunc_t) (void *argument);

/**
* @brief Defines the entry function for timers.
*
* This function is used as a callback when the timer is triggered.
*
* @since 1.0
*/
typedef void (*osTimerFunc_t) (void *argument);

/**
* @brief Enumerates the timer types.
*
* 
*
* @since 1.0
*/
typedef enum {
  /** One-shot timer. */
  osTimerOnce               = 0,
  /** Periodic timer. */
  osTimerPeriodic           = 1
} osTimerType_t;

/**
* @brief Defines a macro that informs the RTOS to wait until a resource becomes available.
*
* 
*
* @since 1.0
*/
#define osWaitForever         0xFFFFFFFFU

/**
* @brief Defines a macro that informs the RTOS to wait until any flag is triggered.
*
* You can use this macro to set the triggering mode in a function.
*
* @since 1.0
*/
#define osFlagsWaitAny        0x00000000U

/**
* @brief Defines a macro that informs the RTOS to wait until all flags are triggered.
*
* You can use this macro to set the triggering mode in a function.
*
* @since 1.0
*/
#define osFlagsWaitAll        0x00000001U

/**
* @brief Defines a macro that informs the RTOS not to clear the flags that have been specified to wait for.
*
* You can use this macro to set the triggering mode in a function.
*
* @since 1.0
*/
#define osFlagsNoClear        0x00000002U

/**
* @brief Defines a macro that indicates an error in the most significant bit of a function. This macro is not supported currently.
*
* @since 1.0
*/
#define osFlagsError          0x80000000U

/**
* @brief Defines a macro that indicates an unknown error for a thread.
*
* @since 1.0
*/
#define osFlagsErrorUnknown   0xFFFFFFFFU

/**
* @brief Defines a macro that indicates a timeout error for a thread.
*
* @since 1.0
*/
#define osFlagsErrorTimeout   0xFFFFFFFEU

/**
* @brief Defines a macro that indicates a resource error for a thread.
*
* @since 1.0
*/
#define osFlagsErrorResource  0xFFFFFFFDU

/**
* @brief Defines a macro that indicates a parameter error for a thread.
*
* @since 1.0
*/
#define osFlagsErrorParameter 0xFFFFFFFCU

/**
* @brief Defines a macro that indicates an ISR error for a thread. 
*
* @since 1.0
*/
#define osFlagsErrorISR       0xFFFFFFFAU

/**
* @brief Defines a macro that indicates a detached thread.
*
* A detached thread cannot be joined by calling {@link osThreadJoin}.
*
* @since 1.0
*/
#define osThreadDetached      0x00000000U

/**
* @brief Defines a macro that indicates a joinable thread.
*
* A joinable thread can be joined by calling {@link osThreadJoin}.
*
* @since 1.0
*/
#define osThreadJoinable      0x00000001U

/**
* @brief Defines a macro that specifies a recursive mutex. This macro is not supported currently.
*
* @since 1.0
*/
#define osMutexRecursive      0x00000001U

/**
* @brief Defines a macro that specifies the mutex priority inheritance attribute. This macro is not supported currently.
*
* @since 1.0
*/
#define osMutexPrioInherit    0x00000002U

/**
* @brief Defines a macro that specifies a robust mutex. Robust mutexes are automatically released when the owning thread is terminated. This macro is not supported currently.
*
* @since 1.0
*/
#define osMutexRobust         0x00000008U

/**
* @brief Enumerates the CMSIS-RTOS return values.
*
* 
*
* @since 1.0
*/
typedef enum {
  /** The operation is successful. */
  osOK                      =  0,
  /** Error without special description. */
  osError                   = -1,
  /** Timeout. */
  osErrorTimeout            = -2,
  /** Resource error. */
  osErrorResource           = -3,
  /** Incorrect parameters. */
  osErrorParameter          = -4,
  /** Insufficient memory. */
  osErrorNoMemory           = -5,
  /** ISR error. */
  osErrorISR                = -6,
  /** Value reserved to prevent from enum down-size compiler optimization. */
  osStatusReserved          = 0x7FFFFFFF
} osStatus_t;

/**
* @brief Defines the thread ID, which identifies different threads.
*
* 
*
* @since 1.0
*/
typedef void *osThreadId_t;

/**
* @brief Defines the timer ID, which identifies different timers.
*
* 
*
* @since 1.0
*/
typedef void *osTimerId_t;

/**
* @brief Defines the event ID, which identifies different events.
*
* 
*
* @since 1.0
*/
typedef void *osEventFlagsId_t;

/**
* @brief Defines the mutex ID, which identifies different mutexes.
*
* 
*
* @since 1.0
*/
typedef void *osMutexId_t;

/**
* @brief Defines the semaphore ID, which identifies different semaphores.
*
* 
*
* @since 1.0
*/
typedef void *osSemaphoreId_t;

/**
* @brief Defines the memory pool ID, which identifies different memory pools.
*
* 
*
* @since 1.0
*/
typedef void *osMemoryPoolId_t;

/**
* @brief Defines the message queue ID, which identifies different message queues.
*
* 
*
* @since 1.0
*/
typedef void *osMessageQueueId_t;


#ifndef TZ_MODULEID_T
#define TZ_MODULEID_T
/**
* @brief Defines the trust zone (TZ) module ID. This parameter is not supported currently.
*
* @since 1.0
*/
typedef uint32_t TZ_ModuleId_t;
#endif

/**
* @brief Defines the thread parameters.
*
* This structure is used to initialize thread configuration. For details about the constraints, see {@link osThreadNew}.
*
* @since 1.0
*/
typedef struct {
  /** Thread name. */
  const char                   *name;
  /** Thread attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the static control block of the thread. */
  void                      *cb_mem;
  /** Size of the static control block of the thread, in bytes. */
  uint32_t                   cb_size;
  /** Pointer to the thread stack. */
  void                   *stack_mem;
  /** Size of the thread stack, in bytes. */
  uint32_t                stack_size;
  /** Thread priority. */
  osPriority_t              priority;
  /** Thread security configuration. */
  TZ_ModuleId_t            tz_module;
  /** Reserved parameter. */
  uint32_t                  reserved;
} osThreadAttr_t;

/**
* @brief Defines the timer parameters. This structure is not supported currently.
*
* @since 1.0
*/
typedef struct {
  /** Timer name. */
  const char                   *name;
  /** Timer attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the timer control block. */
  void                      *cb_mem;
  /** Size of the timer control block. */
  uint32_t                   cb_size;
} osTimerAttr_t;

/**
* @brief Defines the event parameters. This structure is not supported currently.
*
* @since 1.0
*/
typedef struct {
  /** Event name. */
  const char                   *name;
  /** Event attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the event control block. */
  void                      *cb_mem;
  /** Size of the event control block. */
  uint32_t                   cb_size;
} osEventFlagsAttr_t;

/**
* @brief Defines the mutex parameters. This structure is not supported currently.
*
* @since 1.0
*/
typedef struct {
  /** Mutex name. */
  const char                   *name;
  /** Mutex attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the mutex control block. */
  void                      *cb_mem;
  /** Size of the mutex control block. */
  uint32_t                   cb_size;
} osMutexAttr_t;

/**
* @brief Defines the semaphore parameters. This structure is not supported currently.
*
* @since 1.0
*/
typedef struct {
  /** Semaphore name. */
  const char                   *name;
  /** Semaphore attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the semaphore control block. */
  void                      *cb_mem;
  /** Size of the semaphore control block. */
  uint32_t                   cb_size;
} osSemaphoreAttr_t;

/**
* @brief Defines the memory pool parameters.
*
* This structure can be used to initialize the memory pool configuration. For details about the constraints, see {@link osMemoryPoolNew}.
*
* @since 1.0
*/
typedef struct {
  /** Memory pool name. */
  const char                   *name;
  /** Memory pool attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the static control block of the memory pool. */
  void                      *cb_mem;
  /** Size of the static control block of the memory pool, in bytes. */
  uint32_t                   cb_size;
  /** Pointer to the static storage space of the memory pool. */
  void                      *mp_mem;
  /** Size of the static storage space of the memory pool, in bytes. */
  uint32_t                   mp_size;
} osMemoryPoolAttr_t;

/**
* @brief Defines the message queue parameters.
*
* This structure is used to initialize message queue configuration. For details about constraints, see {@link osMessageQueueNew}.
*
* @since 1.0
*/
typedef struct {
  /** Message queue name. */
  const char                   *name;
  /** Message queue attribute bits. */
  uint32_t                 attr_bits;
  /** Pointer to the static control block of the message queue. */
  void                      *cb_mem;
  /** Size of the static control block of the message queue, in bytes. */
  uint32_t                   cb_size;
  /** Pointer to the static memory of the message queue. */
  void                      *mq_mem;
  /** Size of the static memory of the message queue, in bytes. */
  uint32_t                   mq_size;
} osMessageQueueAttr_t;

/**
* @brief Initializes the kernel.
*
* Most APIs cannot be used before the OS initialization.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
osStatus_t osKernelInitialize (void);

/**
* @brief Obtains kernel information, including the kernel version and name.
*
* 
*
* @param version Indicates the pointer to the version information obtained.
* @param id_buf Indicates the pointer to the buffer holding the kernel version information.
* @param id_size Indicates the size of the buffer.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
osStatus_t osKernelGetInfo (osVersion_t *version, char *id_buf, uint32_t id_size);

/**
* @brief Obtains the kernel state.
*
* @return Returns the kernel state obtained. This API does not return {@link osKernelError}.
*
* @since 1.0
*/
osKernelState_t osKernelGetState (void);

/**
* @brief Starts the kernel.
*
* The kernel must be initialized by {@link osKernelInitialize} before being started.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
osStatus_t osKernelStart (void);

/**
* @brief Locks the kernel.
*
* @return Returns the previous kernel state (<b>1</b> means locked and <b>0</b> means unlocked) if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
int32_t osKernelLock (void);

/**
* @brief Unlocks the kernel.
*
* @return Returns the previous kernel state (<b>1</b> means locked and <b>0</b> means unlocked) if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
int32_t osKernelUnlock (void);

/**
* @brief Restores the kernel lock.
*
* @param lock Indicates the lock state set by {@link osKernelLock} or {@link osKernelUnlock}.
*
* @return Returns the previous kernel state (<b>1</b> means locked and <b>0</b> means unlocked) if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
int32_t osKernelRestoreLock (int32_t lock);

/**
* @brief Suspends the kernel. This API is not supported currently.
*
* @since 1.0
*/
uint32_t osKernelSuspend (void);

/**
* @brief Resumes the kernel. This API is not supported currently.
*
* @since 1.0
*/
void osKernelResume (uint32_t sleep_ticks);

/**
* @brief Obtains the number of ticks of the kernel.
*
*
*
* @return Returns the number of ticks obtained.
*
* @since 1.0
*/
uint32_t osKernelGetTickCount (void);

/**
* @brief Obtains the kernel tick frequency, which is the number of ticks per second.
*
* 
*
* @return Returns the tick frequency obtained.
*
* @since 1.0
*/
uint32_t osKernelGetTickFreq (void);

/**
* @brief Obtains the system timer time, that is, the time elapsed after the OS starts.
*
* 
*
* @return Returns the time obtained, in cycles.
*
* @since 1.0
*/
uint32_t osKernelGetSysTimerCount (void);

/**
* @brief Obtains the kernel CPU frequency, that is, the number of CPU cycles per second.
*
* 
*
* @return Returns the CPU frequency obtained, in cycles.
*
* @since 1.0
*/
uint32_t osKernelGetSysTimerFreq (void);

/**
* @brief Creates and initializes a thread.
*
* If the priority of the thread created is higher than that of the thread in running,
* the created thread starts to run immediately.
*
* @param func Indicates the thread callback entry function.
* @param argument Indicates the pointer to the parameters passed to the thread callback.
* @param attr Indicates the pointer to the thread attributes. If <b>NULL</b> or no value is passed in for a certain attribute, the default value of the attribute will be used.
* - <b>name</b> must be <b>NULL</b> or a constant. If it is <b>NULL</b>, the default name will be used.
* - <b>attr_bits</b> is set to {@link osThreadDetached} by default.
* - <b>cb_mem</b> is not supported currently.
* - <b>cb_size</b> is not supported currently.
* - The static stack can be used only when both <b>stack_mem</b> and <b>stack_size</b> are correctly set.
* - If <b>0</b> is passed in for <b>stack_size</b>, the default stack size will be used.
* - The highest priority is {@link osPriorityAboveNormal1}, and the lowest is {@link osPriorityLow}.
* - <b>tz_module</b> is not supported currently.
*
* @return Returns the thread ID if the operation is successful.
* @return Returns <b>NULL</b> if this API fails or is called in an interrupt.
*
* @since 1.0
*/
osThreadId_t osThreadNew (osThreadFunc_t func, void *argument, const osThreadAttr_t *attr);

/**
* @brief Obtains the name of a thread.
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns the thread name obtained if the operation is successful.
* @return Returns <b>NULL</b> if this API fails or is called in an interrupt.
*
* @since 1.0
*/
const char *osThreadGetName (osThreadId_t thread_id);

/**
* @brief Obtains the thread ID.
*
* 
*
* @return Returns the thread ID obtained. This API does not return an error code.
*
* @since 1.0
*/
osThreadId_t osThreadGetId (void);

/**
* @brief Obtains the state of a thread.
*
* 
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns the thread state obtained if the operation is successful.
* @return Returns {@link osThreadError} if this API is called in an interrupt.
*
* @since 1.0
*/
osThreadState_t osThreadGetState (osThreadId_t thread_id);

/**
* @brief Obtains the stack size of a thread.
*
* 
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns the stack size (in bytes) obtained if the operation is successful.
* @return Returns <b>0</b> if this API fails or is called in an interrupt.
*
* @since 1.0
*/
uint32_t osThreadGetStackSize (osThreadId_t thread_id);

/**
* @brief Obtains the stack watermark of a threshold.
*
* This API cannot return the unused stack space through configuration.
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns the stack waterline (in bytes) obtained if the operation is successful.
* @return Returns <b>0</b> if this API fails or is called in an interrupt.
*
* 
*
* @since 1.0
*/
uint32_t osThreadGetStackSpace (osThreadId_t thread_id);

/**
* @brief Sets the priority for a thread.
*
* 
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
* @param priority Indicates the priority to set. The highest priority is {@link osPriorityAboveNormal1}, and the lowest is {@link osPriorityLow}.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorResource} if the thread does not exist.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
*
* @since 1.0
*/
osStatus_t osThreadSetPriority (osThreadId_t thread_id, osPriority_t priority);

/**
* @brief Obtains the priority of a thread.
*
* 
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns the thread priority obtained if the operation is successful.
* @return Returns {@link osPriorityError} if this API has incorrect parameters or is called in an interrupt.
*
* @since 1.0
*/
osPriority_t osThreadGetPriority (osThreadId_t thread_id);

/**
* @brief Passes control to the next thread with the same priority in the <b>Ready</b> state.
*
* This API does not set the current thread to the <b>Blocked</b> state. If there is no other thread with the same priority in the <b>Ready</b> state, the current thread continues execution.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if incorrect parameters are detected or an error occurs.
*
* @since 1.0
*/
osStatus_t osThreadYield (void);

/**
* @brief Suspends a thread.
*
* 
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if the thread is suspended or locked, or does not exist.
*
* @since 1.0
*/
osStatus_t osThreadSuspend (osThreadId_t thread_id);

/**
* @brief Resumes a suspended thread.
*
* The resumed thread transits to the <b>Ready</b> state.
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if the thread is suspended or does not exist.
*
* @since 1.0
*/
osStatus_t osThreadResume (osThreadId_t thread_id);

/**
* @brief Detaches a thread.
*
* This API sets the thread to {@link osThreadDetached}.
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osThreadDetach (osThreadId_t thread_id);

/**
* @brief Waits for a thread to terminate.
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osThreadJoin (osThreadId_t thread_id);

/**
* @brief Terminates the running thread.
*
* 
*
* @return Returns no value.
*
* @since 1.0
*/
__NO_RETURN void osThreadExit (void);

/**
* @brief Terminates a thread.
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if the thread does not exist.
*
* @since 1.0
*/
osStatus_t osThreadTerminate (osThreadId_t thread_id);

/**
* @brief Obtains the number of threads in use.
*
* 
*
* @return Returns the number of threads obtained if the operation is successful.
* @return Returns <b>0</b> if this API is called in an interrupt.
*
* @since 1.0
*/
uint32_t osThreadGetCount (void);

/**
* @brief Obtains active threads. This API is not supported currently.
*
* @since 1.0
*/
uint32_t osThreadEnumerate (osThreadId_t *thread_array, uint32_t array_items);

/**
* @brief Sets thread flags for a thread.
*
* 
*
* @param thread_id Indicates the ID of the target thread. The thread ID can be created by {@link osThreadNew} or obtained by {@link osThreadGetId}.
* @param flags Indicates the flags to set. The 25th bit cannot be <b>1</b>. Otherwise, a parameter error will be returned.
*
* @return Returns the thread flags set if the operation is successful.
* @return Returns {@link osFlagsErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osFlagsErrorResource} if any other error occurs.
*
* @since 1.0
*/
uint32_t osThreadFlagsSet (osThreadId_t thread_id, uint32_t flags);

/**
* @brief Clears the specified flags for the running thread.
*
* 
*
* @param flags Indicates the flags to clear. The 25th bit of the thread flag is the error flag.
*
* @return Returns the thread flags before clearing if the operation is successful.
* @return Returns {@link osFlagsErrorUnknown} if this API is called in an interrupt.
* @return Returns {@link osFlagsErrorResource} if any other error occurs.
*
* @since 1.0
*/
uint32_t osThreadFlagsClear (uint32_t flags);

/**
* @brief Obtains the flags of the running thread.
*
* 
*
* @return Returns the flags obtained if the operation is successful.
* @return Returns {@link osFlagsErrorUnknown} if this API is called in an interrupt.
*
* @since 1.0
*/
uint32_t osThreadFlagsGet (void);

/**
* @brief Waits for the specified thread flags to become signaled.
*
* 
*
* @param flags Indicates the flags to wait for. The 25th bit is the error flag.
* @param options Indicates the wait condition, which can be {@link osFlagsWaitAny}, {@link osFlagsWaitAll}, or {@link osFlagsNoClear}.
* @param timeout Indicates the timeout interval, in ticks.
*
* @return Returns the thread flags before clearing if the operation is successful.
* @return Returns {@link osFlagsErrorUnknown} if this API is called in an interrupt.
* @return Returns {@link osFlagsErrorParameter} if <b>flag</b> or <b>options</b> is incorrect.
* @return Returns {@link osFlagsErrorTimeout} if the awaited flags have not been set in the given time.
* @return Returns {@link osFlagsErrorResource} if any other error occurs.
*
* @since 1.0
*/
uint32_t osThreadFlagsWait (uint32_t flags, uint32_t options, uint32_t timeout);

/**
* @brief Waits for timeout.
*
* You can use this API to put the thread to the <b>Suspended</b> state, and a context switch occurs immediately. \n
* You can also provide a <b>VOID HalDelay(UINT32 ticks)</b>
* to enable this API to be called before the system starts.
*
* @param ticks Indicates the time to wait, in ticks.
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorParameter} if <b>ticks</b> is <b>0</b>.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
osStatus_t osDelay (uint32_t ticks);

/**
* @brief Waits until the specified time reaches.
*
*
*
* @param ticks Indicates the absolute time to wait, in ticks. It cannot be earlier than the current time.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osError} if an error occurs.
*
* @since 1.0
*/
osStatus_t osDelayUntil (uint32_t ticks);

/**
* @brief Creates a timer.
*
* The timer is in the <b>Stopped</b> state until it is started by {@link osTimerStart}.
*
* @param func Indicates the callback to be invoked.
* @param type Indicates the timer type, which can be {@link osTimerOnce} or {@link osTimerPeriodic}.
* @param argument Indicates the pointer to the timer callback parameter.
* @param attr Indicates the pointer to the timer attributes. It can be <b>NULL</b>. This parameter is not supported currently.
*
* @return Returns the timer ID if the operation is successful.
* @return Returns <b>NULL</b> if this API fails or is called in an interrupt.
*
* @since 1.0
*/
osTimerId_t osTimerNew (osTimerFunc_t func, osTimerType_t type, void *argument, const osTimerAttr_t *attr);

/**
* @brief Obtains the timer name.
*
* @param timer_id Indicates the ID of the target timer (created by {@link osTimerNew}).
*
* @return Returns only <b>NULL</b> currently.
* @since 1.0
*/
const char *osTimerGetName (osTimerId_t timer_id);

/**
* @brief Starts a timer.
*
* 
*
* @param timer_id Indicates the ID of the target timer (created by {@link osTimerNew}).
* @param ticks Indicates the value of the timer, in ticks.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osTimerStart (osTimerId_t timer_id, uint32_t ticks);

/**
* @brief Stops a timer.
*
* @param timer_id Indicates the ID of the target timer (created by {@link osTimerNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osTimerStop (osTimerId_t timer_id);

/**
* @brief Checks whether a timer is running.
*
* 
*
* @param timer_id Indicates the ID of the target timer (created by {@link osTimerNew}).
*
* @return Returns the timer state (<b>1</b> means running, and <b>0</b> means the opposite) if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns <b>0</b> if the input parameter is <b>NULL</b>.
*
* @since 1.0
*/
uint32_t osTimerIsRunning (osTimerId_t timer_id);

/**
* @brief Deletes a timer.
*
* 
*
* @param timer_id Indicates the ID of the target timer (created by {@link osTimerNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osTimerDelete (osTimerId_t timer_id);

/**
* @brief Creates and initializes an event.
*
* @param attr Indicates the pointer to the event attributes. It can be <b>NULL</b>. This parameter is not supported currently.
*
* @return Returns the event ID if the operation is successful.
* @return Returns <b>NULL</b> if this API fails or is called in an interrupt.
* @since 1.0
*/
osEventFlagsId_t osEventFlagsNew (const osEventFlagsAttr_t *attr);

/**
* @brief Obtains the event name.
*
* @param ef_id Indicates the ID of the target event (created by {@link osEventFlagsNew}).
*
* @return Returns only <b>NULL</b> currently.
*
* @since 1.0
*/
const char *osEventFlagsGetName (osEventFlagsId_t ef_id);

/**
* @brief Sets event flags.
*
* @param ef_id Indicates the ID of the target event (created by {@link osEventFlagsNew}).
* @param flags Indicates the flags to set. The 25th bit cannot be <b>1</b>.
*
* @return Returns the event flags set if the operation is successful.
* @return Returns {@link osFlagsErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osFlagsErrorResource} if any other error occurs.
*
* @since 1.0
*/
uint32_t osEventFlagsSet (osEventFlagsId_t ef_id, uint32_t flags);

/**
* @brief Clears the flags of an event.
*
* 
*
* @param ef_id Indicates the ID of the target event (created by {@link osEventFlagsNew}).
* @param flags Indicates the flags to clear.
*
* @return Returns the event flags before clearing if the operation is successful.
* @return Returns {@link osFlagsErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osFlagsErrorResource} if any other error occurs.
*
* @since 1.0
*/
uint32_t osEventFlagsClear (osEventFlagsId_t ef_id, uint32_t flags);

/**
* @brief Obtains the flags of an event.
*
* @param ef_id Indicates the ID of the target event (created by {@link osEventFlagsNew}).
*
* @return Returns the event flags obtained if the operation is successful.
* @return Returns <b>0</b> if the operation fails.
*
* @since 1.0
*/
uint32_t osEventFlagsGet (osEventFlagsId_t ef_id);

/**
* @brief Suspends the current thread until the specified event flags are set.
*
* @param ef_id Indicates the ID of the target event (created by {@link osEventFlagsNew}).
* @param flags Indicates the flags to wait for. The 25th bit cannot be <b>1</b>.
* @param options Indicates the wait condition, which can be {@link osFlagsWaitAny}, {@linkosFlagsWaitAll}, or {@link osFlagsNoClear}.
* @param timeout Indicates the timeout interval, in ticks.
*
* @return Returns the value, which varies depending on the <b>options</b> value, if the operation is successful.
* @return Returns {@link osFlagsErrorParameter} if incorrect parameters are detected or this API is called in an interrupt.
* @return Returns {@link osFlagsErrorTimeout} if the specified flags have not been set in the given time.
* @return Returns {@link osFlagsErrorResource} if any other error occurs.
*
* @since 1.0
*/
uint32_t osEventFlagsWait (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t timeout);

/**
* @brief Deletes an event.
*
* 
*
* @param ef_id Indicates the ID of the target event (created by {@link osEventFlagsNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osFlagsErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
*
* @since 1.0
*/
osStatus_t osEventFlagsDelete (osEventFlagsId_t ef_id);

/**
* @brief Creates and initializes a mutex.
*
* @param attr Indicates the pointer to the mutex attributes. It can be <b>NULL</b>. This parameter is not supported currently.
*
* @return Returns the mutex ID if the operation is successful.
* @return Returns <b>NULL</b> if this API fails or is called in an interrupt.
*
* @since 1.0
*/
osMutexId_t osMutexNew (const osMutexAttr_t *attr);

/**
* @brief Obtains the mutex name. This API is not supported currently.
*
* @since 1.0
*/
const char *osMutexGetName (osMutexId_t mutex_id);

/**
* @brief Acquires a mutex.
*
* The running thread will be blocked until the mutex is acquired or the timeout timer has expired.
*
* @param mutex_id Indicates the ID of the target mutex (created by {@link osMutexNew}).
* @param timeout Indicates the maximum time to wait to acquire the mutex, in ticks.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorTimeout} if the mutex has not been acquired in the given time.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMutexAcquire (osMutexId_t mutex_id, uint32_t timeout);

/**
* @brief Releases a mutex.
*
* The threads waiting for this mutex will transit to the <b>Ready</b> state.
*
* @param mutex_id Indicates the ID of the target mutex (created by {@link osMutexNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMutexRelease (osMutexId_t mutex_id);

/**
* @brief Obtains the thread that acquired the specified mutex.
*
* @param mutex_id Indicates the ID of the target mutex (created by {@link osMutexNew}).
*
* @return Returns the thread ID if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or this API is called in an interrupt.
*
* @since 1.0
*/
osThreadId_t osMutexGetOwner (osMutexId_t mutex_id);

/**
* @brief Deletes a mutex.
*
* 
*
* @param mutex_id Indicates the ID of the target mutex (created by {@link osMutexNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMutexDelete (osMutexId_t mutex_id);

/**
* @brief Creates and initializes a semaphore.
*
* 
*
* @param max_count Indicates the maximum number of available tokens. When <b>max_count</b> is <b>1</b>, a binary semaphore is created.
* @param initial_count Indicates the initial number of available tokens.
* @param attr Indicates the pointer to the semaphore attributes. It can be <b>NULL</b>. This parameter is not supported currently.
*
* @return Returns the semaphore ID if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or this API is called in an interrupt.
*
* @since 1.0
*/
osSemaphoreId_t osSemaphoreNew (uint32_t max_count, uint32_t initial_count, const osSemaphoreAttr_t *attr);

/**
* @brief Obtains the semaphore name.
*
* @param semaphore_id Indicates the ID of the target semaphore (created by {@link osSemaphoreNew}).
*
* @return Returns only <b>NULL</b> currently.
*
* @since 1.0
*/
const char *osSemaphoreGetName (osSemaphoreId_t semaphore_id);

/**
* @brief Acquires a semaphore.
*
* The running thread will be blocked until the semaphore is acquired or the timeout timer has expired.
*
* @param semaphore_id Indicates the ID of the target semaphore (created by {@link osSemaphoreNew}).
* @param timeout Indicates the timeout interval, in ticks. This parameter must be <b>0</b> if this API is called in an interrupt.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorTimeout} if the semaphore has not been acquired in the given time.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osSemaphoreAcquire (osSemaphoreId_t semaphore_id, uint32_t timeout);

/**
* @brief Releases a semaphore.
*
* The threads waiting for this semaphore will transit to the <b>Ready</b> state.
*
* @param semaphore_id Indicates the ID of the target semaphore (created by {@link osSemaphoreNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osSemaphoreRelease (osSemaphoreId_t semaphore_id);

/**
* @brief Obtains the number of available tokens of a semaphore.
*
* 
*
* @param semaphore_id Indicates the ID of the target semaphore (created by {@link osSemaphoreNew}).
*
* @return Returns the number of available tokens obtained (which may be <b>0</b>) if the operation is successful.
* @return Returns <b>0</b> if the operation fails.
*
* @since 1.0
*/
uint32_t osSemaphoreGetCount (osSemaphoreId_t semaphore_id);

/**
* @brief Deletes a semaphore.
*
* @param semaphore_id Indicates the ID of the target semaphore (created by {@link osSemaphoreNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected or this API is called in an interrupt.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osSemaphoreDelete (osSemaphoreId_t semaphore_id);

/**
* @brief Creates and initializes a memory pool.
*
* 
*
* @param block_count Indicates the number of blocks in the memory pool.
* @param block_size Indicates the memory block size, in bytes.
* @param attr Indicates the pointer to the memory pool attributes. If the parameter is set to <b>NULL</b>, default values will be used.
* - <b>name</b> is not supported currently.
* - <b>attr_bits</b> is not supported currently.
* - If <b>cb_mem</b> is not <b>NULL</b> and <b>cb_size</b> is greater than the size required by the control block, the memory control blocks passed in can be used.
* The size of the memory control block (<b>MemPoolCB</b>) must be 36 bytes. For details about <b>MemPoolCB</b>, see <b>cmsis_liteos2.c</b>.
* - If <b>mp_mem</b> is not <b>NULL</b> and the pointed address is a 4-byte pair, the memory pool passed in can be used.
*
* @return Returns the memory pool ID if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or this API is called in an interrupt.
*

*
* @since 1.0
*/
osMemoryPoolId_t osMemoryPoolNew (uint32_t block_count, uint32_t block_size, const osMemoryPoolAttr_t *attr);

/**
* @brief Obtains the name of a memory pool.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
*
* @return Returns the memory pool name obtained if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or this API is called in an interrupt.
*
* @since 1.0
*/
const char *osMemoryPoolGetName (osMemoryPoolId_t mp_id);

/**
* @brief Allocates memory from a memory pool.
*
* This API does not block the thread.
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
* @param timeout Indicates the timeout interval. This parameter is not supported currently.
*
* @return Returns the address of the memory block allocated if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or this API is called in an interrupt.
*
* @since 1.0
*/
void *osMemoryPoolAlloc (osMemoryPoolId_t mp_id, uint32_t timeout);

/**
* @brief Releases a memory block.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
* @param block Indicates the pointer to the memory block to release.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMemoryPoolFree (osMemoryPoolId_t mp_id, void *block);

/**
* @brief Obtains the maximum number of blocks in a memory pool.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
*
* @return Returns the maximum number of blocks if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMemoryPoolGetCapacity (osMemoryPoolId_t mp_id);

/**
* @brief Obtains the memory block size of a memory pool.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
*
* @return Returns the block size obtained if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMemoryPoolGetBlockSize (osMemoryPoolId_t mp_id);

/**
* @brief Obtains the number of used memory blocks in a memory pool.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
*
* @return Returns the number of used blocks if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMemoryPoolGetCount (osMemoryPoolId_t mp_id);

/**
* @brief Obtains the number of available memory blocks in a memory pool.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
*
* @return Returns the number of available memory blocks if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMemoryPoolGetSpace (osMemoryPoolId_t mp_id);

/**
* @brief Deletes a memory pool.
*
* 
*
* @param mp_id Indicates the ID of the target memory pool (created by {@link osMemoryPoolNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMemoryPoolDelete (osMemoryPoolId_t mp_id);


/**
* @brief Creates and initializes a message queue.
*
* Before using the external memory to store message queues, enable <b>LOSCFG_BASE_IPC_QUEUE_STATIC</b>.
*
* @param msg_count Indicates the maximum number of messages in the queue.
* @param msg_size Indicates the maximum message size in bytes. If static memory is used to store message queues, this parameter does not take effect and cannot be set to <b>0</b>.
* @param attr Indicates the pointer to message queue attributes. If this parameter is set to <b>NULL</b>, default values will be used. By default, dynamic memory is used.
* - <b>name</b> must be <b>NULL</b> or a constant.
* - <b>attr_bits</b> is not supported currently.
* - <b>cb_mem</b> does not support configuration of static control blocks currently.
* - <b>cb_size</b> does not support configuration of static control blocks currently.
* - If <b>mq_mem</b> is not <b>NULL</b> and <b>mq_size</b> is not <b>0</b>, static memory is used for message queues.
* - If <b>mq_mem</b> is <b>NULL</b> and <b>mq_size</b> is <b>0</b>, dynamic memory is used for message queues.
* - <b>mq_size</b> specifies the size of the entire static memory, which is <b>msg_size</b> multiplied by <b>msg_count</b>.
*
* @return Returns the ID of the message queue created if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or this API is called in an interrupt.
*
* @since 1.0
*/
osMessageQueueId_t osMessageQueueNew (uint32_t msg_count, uint32_t msg_size, const osMessageQueueAttr_t *attr);

/**
* @brief Obtains the name of a message queue.
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
*
* @return Returns the message queue name obtained if the operation is successful.
* @return Returns <b>NULL</b> if incorrect parameters are detected or the message queue name is not set.
*
* @since 1.0
*/
const char *osMessageQueueGetName (osMessageQueueId_t mq_id);

/**
* @brief Puts a message into a message queue.
*
* Currently, the message priority is not supported, and a new message is put at the end of the queue.
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
* @param msg_ptr Indicates the pointer to the buffer holding the message to put into the queue.
* @param msg_prio Indicates the message priority. This parameter is not supported currently.
* @param timeout Indicates the timeout interval, in ticks. This parameter must be <b>0</b> if this API is called in an interrupt.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorTimeout} if the message has not been put into the queue in the given time.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMessageQueuePut (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout);

/**
* @brief Retrieves a message from a message queue.
*
* Currently, the message priority is not supported.
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
* @param msg_ptr Indicates the pointer to the buffer holding the message to get from the queue.
* @param msg_prio Indicates the message priority. This parameter is not supported currently.
* @param timeout Indicates the timeout interval, in ticks. This parameter must be <b>0</b> if this API is called in an interrupt.
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorTimeout} if the message has not been retrieved from the queue in the given time.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMessageQueueGet (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout);

/**
* @brief Obtains the maximum number of messages in a message queue.
*
* 
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
*
* @return Returns the maximum number of messages in the queue if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMessageQueueGetCapacity (osMessageQueueId_t mq_id);

/**
* @brief Obtains the maximum message size (in bytes) in a message queue.
*
* 
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
*
* @return Returns the maximum message size if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMessageQueueGetMsgSize (osMessageQueueId_t mq_id);

/**
* @brief Obtains the number of queued messages in a message queue.
*
* 
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
*
* @return Returns the number of queued messages if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMessageQueueGetCount (osMessageQueueId_t mq_id);

/**
* @brief Obtains the number of available slots for messages in a message queue.
*
*
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
*
* @return Returns the number of available slots for messages if the operation is successful.
* @return Returns <b>0</b> if incorrect parameters are detected.
*
* @since 1.0
*/
uint32_t osMessageQueueGetSpace (osMessageQueueId_t mq_id);

/**
* @brief Resets a message queue. This API is not supported currently.
*
* @since 1.0
*/
osStatus_t osMessageQueueReset (osMessageQueueId_t mq_id);

/**
* @brief Deletes a message queue.
*
* @param mq_id Indicates the ID of the target message queue (created by {@link osMessageQueueNew}).
*
* @return Returns {@link osOK} if the operation is successful.
* @return Returns {@link osErrorISR} if this API is called in an interrupt.
* @return Returns {@link osErrorParameter} if incorrect parameters are detected.
* @return Returns {@link osErrorResource} if any other error occurs.
*
* @since 1.0
*/
osStatus_t osMessageQueueDelete (osMessageQueueId_t mq_id);


#ifdef  __cplusplus
}
#endif

#endif  // CMSIS_OS2_H_

/** @} */
