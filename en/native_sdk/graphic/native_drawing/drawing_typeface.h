/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TYPEFACE_H
#define C_INCLUDE_DRAWING_TYPEFACE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_typeface.h
 *
 * @brief Declares the functions related to the typeface in the drawing module.
 * Different platforms have their own default typefaces. You can also parse the .ttf file to obtain the typefaces
 * specified by the third party, such as SimSun and SimHei.
 *
 * File to include: native_drawing/drawing_typeface.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object, which defines the default font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Typeface</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateDefault(void);

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object through a file.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the file path.
 * @param index File index.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateFromFile(const char* path, int index);

/**
 * @brief Creates an <b>OH_Drawing_Typeface</b> object through a memory stream.
 * If the memory stream is an invalid font file, a null pointer is returned.
 * After the memory stream is passed in, the ownership is transferred and you cannot release it.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_MemoryStream Pointer to an {@link OH_Drawing_MemoryStream} object.
 * @param index Index of the memory stream.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_TypefaceCreateFromStream(OH_Drawing_MemoryStream*, int32_t index);

/**
 * @brief Destroys an <b>OH_Drawing_Typeface</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typeface Pointer to an <b>OH_Drawing_Typeface</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TypefaceDestroy(OH_Drawing_Typeface*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
