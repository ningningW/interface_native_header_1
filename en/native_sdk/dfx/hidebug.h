/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIVIEWDFX_HIDEBUG_H
#define HIVIEWDFX_HIDEBUG_H
/**
 * @addtogroup HiDebug
 * @{
 *
 * @brief Provides the debugging function.
 *
 * The functions of this module can be used to obtain information such as the CPU usage, memory,
 * heap, and capture trace.
 *
 * @since 12
 */

/**
 * @file hideug.h
 *
 * @brief Defines the debugging function of the HiDebug module.
 *
 * @library libohhidebug.so
 * @syscap SystemCapability.HiviewDFX.HiProfiler.HiDebug
 * @since 12
 */

#include <stdint.h>
#include "hidebug_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Obtains the CPU usage of the system.
 *
 * @return CPU usage of the system.
 * @since 12
 */
double OH_HiDebug_GetSystemCpuUsage();

/**
 * @brief Obtains the CPU usage of a process.
 *
 * @return CPU usage of a process.
 * @since 12
 */
double OH_HiDebug_GetAppCpuUsage();

/**
 * @brief Obtains the CPU usage of all threads of an application.
 *
 * @return CPU usage of all threads. For details, see {@link HiDebug_ThreadCpuUsagePtr}.
 * @since 12
 */
HiDebug_ThreadCpuUsagePtr OH_HiDebug_GetAppThreadCpuUsage();

/**
 * @brief Releases the thread data structure.
 *
 * @param threadCpuUsage CPU usage of all threads of an application. For details,
 * see {@link HiDebug_ThreadCpuUsagePtr}.
 * @since 12
 */
void OH_HiDebug_FreeThreadCpuUsage(HiDebug_ThreadCpuUsagePtr *threadCpuUsage);

/**
 * @brief Obtains system memory information.
 *
 * @param systemMemInfo System memory information, which points to {@link HiDebug_SystemMemInfo}.
 * @since 12
 */
void OH_HiDebug_GetSystemMemInfo(HiDebug_SystemMemInfo *systemMemInfo);

/**
 * @brief Obtains the memory information of an application process.
 *
 * @param nativeMemInfo Native memory information, which points to {@link HiDebug_NativeMemInfo}.
 * @since 12
 */
void OH_HiDebug_GetAppNativeMemInfo(HiDebug_NativeMemInfo *nativeMemInfo);

/**
 * @brief Obtains the memory limit of an application process.
 *
 * @param memoryLimit Memory limit, which points to {@link HiDebug_MemoryLimit}.
 * @since 12
 */
void OH_HiDebug_GetAppMemoryLimit(HiDebug_MemoryLimit *memoryLimit);

/**
 * @brief Starts application trace collection.
 *
 * @param flag Flag for thread trace collection.
 * @param tags Tags for trace collection scenarios.
 * @param limitSize Maximum size of a trace file, in bytes. The maximum size is 500 MB.
 * @param fileName Output trace file name.
 * @param length Length of the trace file name.
 * @return {@code HIDEBUG_SUCCESS} if the operation is successful. For details, see {@link HiDebug_ErrorCode}.
 * @since 12
 */
HiDebug_ErrorCode OH_HiDebug_StartAppTraceCapture(HiDebug_TraceFlag flag, uint64_t tags, uint32_t limitSize,
    char* fileName, uint32_t length);

/**
 * @brief Stops application trace collection.
 *
 * @return {@code HIDEBUG_SUCCESS} if the operation is successful. For details, see {@link HiDebug_ErrorCode}.
 * @since 12
 */
HiDebug_ErrorCode OH_HiDebug_StopAppTraceCapture();

#ifdef __cplusplus
}
#endif
/** @} */

#endif // HIVIEWDFX_HIDEBUG_H
